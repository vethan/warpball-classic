﻿using UnityEngine;
using System.Collections;

public class PhotonTester : MonoBehaviour {

	// Use this for initialization
	void Start () {
		PhotonNetwork.logLevel = PhotonLogLevel.Full;
		PhotonNetwork.ConnectUsingSettings("Test");


	}
	void OnJoinedLobby()
	{
		print("in a lobby");
		PhotonNetwork.JoinRandomRoom();
		/*
		RoomOptions r = new RoomOptions();
		r.isOpen = true;
		r.isVisible = true;
		r.maxPlayers = 4;
		PhotonNetwork.JoinOrCreateRoom("TestRoom",r,TypedLobby.Default);*/
	}

	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("Can't join random room!");
		PhotonNetwork.CreateRoom(null);
	}

	void OnJoinedRoom()
	{
		print("in a room");
		print(PhotonNetwork.room.name);
	}

	// Update is called once per frame
	void Update () {
		//print(PhotonNetwork.connectionState);
	}
}
