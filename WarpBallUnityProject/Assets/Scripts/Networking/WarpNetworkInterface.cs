using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PhotonView))]
public abstract class WarpNetworkInterface : Photon.MonoBehaviour
{
	public const float DELAY_AMOUNT = 0.13f;
	protected int ownerID = -1;
	protected SortedList<double,WarpNetworkState> pastStates = new SortedList<double, WarpNetworkState>();

	protected enum CloseDirection {Forward, Backward};

	public bool isMine
	{
		get
		{
			return photonView.isMine || (photonView.isSceneView && PhotonNetwork.player.isMasterClient);
		}
	}

	protected WarpNetworkState GetClosestState(double time, CloseDirection direction)
	{
		if(pastStates.Count == 0)
			return null;

		for(int i = 0; i < pastStates.Count; i++)
		{
			if(pastStates.Keys[i] > time)
			{
				if(direction == CloseDirection.Forward || i == 0)
				{
					return pastStates.Values[i];
				}
				else
				{
					return pastStates.Values[i-1];
				}
			}
		}
		return pastStates.Values[pastStates.Count-1];
	}

	void RemoveOldStates()
	{
		//Neaten up the state list
		double targetTime = PhotonNetwork.time - (2 * DELAY_AMOUNT);
		while(pastStates.Count > 2 && pastStates.Keys[0] < targetTime)
		{
			pastStates.RemoveAt(0);
		}
	}

	protected virtual void LateUpdate()
	{
		RemoveOldStates();
	}

}


public interface WarpNetworkState
{
	double timeStamp
	{
		get;
	}
}

