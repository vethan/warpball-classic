using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Fabric;

[RequireComponent(typeof(Ball))]
public class WarpBallobjectNetworkInterface : WarpNetworkInterface 
{
	Transform mTrans;
	Ball mBall;
	Rigidbody2D mRigidbody;
	
	// Use this for initialization
	void Awake () 
	{
		mTrans = transform;
		mBall = GetComponent<Ball>();
		mBall.SetInterface(this);
		mRigidbody = rigidbody2D;
		gameObject.layer = LayerMask.NameToLayer("Balls");
	}

	[RPC]
	void AddNewState(double t, Vector3 position, Vector2 velocity)
	{
		WarpBallobjectNetworkState state = new WarpBallobjectNetworkState(t,position,velocity);		
		if(!pastStates.ContainsKey(t))
			pastStates.Add(t,state);
	}


	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			// We own this ball: send the others our data
			double t = PhotonNetwork.time;
			stream.SendNext(t);
			stream.SendNext(mTrans.position);
			stream.SendNext(mRigidbody.velocity);
		}
		else
		{
			// Network ball, receive data
			AddNewState((double)stream.ReceiveNext(),
			            (Vector3)stream.ReceiveNext(),
			            (Vector2)stream.ReceiveNext());

		}
	}

	public void SendVelocityChange(Vector3 positionChangedFrom, Vector2 newVelocity)
	{
		photonView.RPC("ProcessImmediateChange",PhotonTargets.MasterClient, positionChangedFrom, newVelocity);
	}

	void Update()
	{
		if(isMine)
			return;
		double currentTime = PhotonNetwork.time;
		double targetTime = currentTime-DELAY_AMOUNT;
		WarpBallobjectNetworkState startState = (WarpBallobjectNetworkState)GetClosestState(targetTime,CloseDirection.Backward);
		if(startState==null)
		{
			return;
		}

		WarpBallobjectNetworkState endState = (WarpBallobjectNetworkState)GetClosestState(targetTime,CloseDirection.Forward);
		WarpBallobjectNetworkState curState = WarpBallobjectNetworkState.Interpolate(startState,endState,targetTime);
		mRigidbody.velocity = curState.velocity;
		mTrans.position = curState.position;

	}

	[RPC]
	void ProcessImmediateChange(Vector3 positionChangedFrom, Vector2 newVelocity)
	{
		mRigidbody.velocity = newVelocity;
	}
}


public class WarpBallobjectNetworkState : WarpNetworkState
{
	double mTimestamp = 0;
	Vector3 mPosition;
	Vector2 mVelocity;

	public static WarpBallobjectNetworkState Interpolate(WarpBallobjectNetworkState start, WarpBallobjectNetworkState end, double time)
	{
		if(time <= start.mTimestamp)
			return start;

		if(time >= end.mTimestamp)
			return end;

		float tScale = (float)((time-start.mTimestamp)/(end.mTimestamp-start.mTimestamp));
		return new WarpBallobjectNetworkState(time,
		                                  Vector3.Lerp(start.mPosition,end.mPosition,tScale),
		                                  Vector2.Lerp(start.mVelocity,end.mVelocity,tScale));
	}

	public double timeStamp
	{
		get
		{
			return mTimestamp;
		}
		set
		{
			mTimestamp = value;
		}
	}

	public Vector3 position
	{
		get
		{
			return mPosition;
		}
	}

	public Vector2 velocity
	{
		get
		{
			return mVelocity;
		}
	}
	public WarpBallobjectNetworkState(double time, Vector3 pos, Vector2 vel)
	{
		mTimestamp = time;
		mPosition = pos;
		mVelocity = vel;
	}
}