using UnityEngine;
using System.Collections;
using Fabric;

[RequireComponent(typeof(WarpPlayer))]
public class WarpPlayerNetworkInterface : WarpNetworkInterface 
{	
	int mWarpCount = 0;
	Transform mTrans;
	WarpPlayer mPlayer;
	Rigidbody2D mRigidbody;
	//public float MAX_ERROR_DISTANCE = 0.7f;
	//public float CORRECTION_AMOUNT = 0.1f;
	// Use this for initialization
	void Awake () 
	{
		mTrans = transform;
		mPlayer = GetComponent<WarpPlayer>();
		mRigidbody = rigidbody2D;
		/*if(!isMine)
			mRigidbody.isKinematic = true;*/
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(PhotonNetwork.time);
			stream.SendNext(mWarpCount);
			stream.SendNext(mTrans.position);
			stream.SendNext(mRigidbody.velocity);
		}
		else
		{
			// Network player, receive data
			WarpPlayerNetworkState state = new WarpPlayerNetworkState((double)stream.ReceiveNext(),
			                                                          (int)stream.ReceiveNext(),
			                                                          (Vector3)stream.ReceiveNext(),
			                                                          (Vector2)stream.ReceiveNext());

			pastStates.Add(state.timeStamp,state);
		}
	}

	public void DidWarp()
	{
		mWarpCount++;
	}

	[RPC]
	public void DoBlast()
	{
		BlastParticlePool.CreateBlastEffect(mTrans.position, mPlayer.color);
		EventManager.Instance.PostEvent("BlastSound");
		ShakeCamera.Shake(WarpPlayer.BLAST_SHAKE_AMOUNT);
	}
	
	void SendBlast()
	{
		photonView.RPC("DoBlast",PhotonTargets.Others);
	}

	void Update()
	{
		if(PhotonNetwork.isMasterClient)
		{
			return;
		}

		double targetTime = PhotonNetwork.time -DELAY_AMOUNT;
		WarpPlayerNetworkState startState = (WarpPlayerNetworkState)GetClosestState(targetTime,CloseDirection.Backward);
		if(startState==null)
		{
			return;
		}
		WarpPlayerNetworkState endState = (WarpPlayerNetworkState)GetClosestState(targetTime,CloseDirection.Forward);
		
		WarpPlayerNetworkState curState = WarpPlayerNetworkState.Interpolate(startState,endState,targetTime);
		if(mWarpCount < curState.warpCount)
		{
			Vector3 oldPos = mTrans.position;
			TeleportParticlePool.CreateTeleportEffect(oldPos,curState.position, mPlayer.color);
			
			if((oldPos-curState.position).magnitude>0.2f)
			{
				EventManager.Instance.PostEvent("WarpSound");
			}
			mWarpCount = curState.warpCount;
		}

		/*if(mNetInput.isMine)
		{
			if((mTrans.position-curState.position).magnitude >= MAX_ERROR_DISTANCE)
			{
				mTrans.position = curState.position;
				mRigidbody.velocity = curState.velocity;
			}
			else
			{
				mTrans.position = Vector3.MoveTowards(mTrans.position,curState.position,CORRECTION_AMOUNT*Time.deltaTime);
			}
		}
		else*/
		{
			mTrans.position = curState.position;
			mRigidbody.velocity = curState.velocity;
		}
	}
}

public class WarpPlayerNetworkState : WarpNetworkState
{
	double mTimestamp = 0;
	int mWarpCount = 0;
	Vector3 mPosition;
	Vector2 mVelocity;
	
	public static WarpPlayerNetworkState Interpolate(WarpPlayerNetworkState start, WarpPlayerNetworkState end, double time)
	{
		if(time <= start.mTimestamp)
			return start;
		
		if(time >= end.mTimestamp)
			return end;
		
		float tScale = (float)((time-start.mTimestamp)/(end.mTimestamp-start.mTimestamp));

		if(end.warpCount > start.warpCount)
		{
			if(tScale <= 0.5f)
			{
				return start;
			}
			else
			{
				return end;
			}
		}

		return new WarpPlayerNetworkState(time,
		                                  end.warpCount,
		                                  Vector3.Lerp(start.mPosition,end.mPosition,tScale),
		                                  Vector2.Lerp(start.mVelocity,end.mVelocity,tScale));
	}
	
	public double timeStamp
	{
		get
		{
			return mTimestamp;
		}
	}

	public int warpCount
	{
		get
		{
			return mWarpCount;
		}
	}

	public Vector3 position
	{
		get
		{
			return mPosition;
		}
	}
	
	public Vector2 velocity
	{
		get
		{
			return mVelocity;
		}
	}
	public WarpPlayerNetworkState(double time, int warpCnt, Vector3 pos, Vector2 vel)
	{
		mTimestamp = time;
		mWarpCount = warpCnt;
		mPosition = pos;
		mVelocity = vel;
	}

}