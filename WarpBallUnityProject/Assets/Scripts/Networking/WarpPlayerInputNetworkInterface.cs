using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WarpPlayerInputNetworkInterface : Photon.MonoBehaviour
{
	WarpPlayer mPlayer;
	PhotonView mInputView;
	PhotonView mRigidbodyView;

	NetworkPlayerController mController;
	WarpPlayerNetworkInterface mWarpNetworkInterface;
	public bool isMine
	{
		get
		{
			return mInputView.isMine;
		}
	}

	void Awake () 
	{
		mPlayer = GetComponent<WarpPlayer>();
		mInputView = photonView;
		mController = new NetworkPlayerController();
		mPlayer.SetNetworkInterface(this);
		if(!mInputView.isMine)
		{
			mPlayer.SetInput(mController,true);
		}

		if(PhotonNetwork.isMasterClient)
		{
			mInputView.RPC("SetupRigidbodyView",PhotonTargets.AllBuffered,PhotonNetwork.AllocateViewID());
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			// We own this player: send the others our data
			PlayerControlStatus s = mPlayer.controllerState;
			stream.SendNext(s.leftStick);
			stream.SendNext(s.rightStick);
			stream.SendNext(s.warpButton);
			stream.SendNext(s.blastButton);
		}
		else
		{
			// Network player, receive data
			mController.SetNewState((Vector2)stream.ReceiveNext(),
			                        (Vector2)stream.ReceiveNext(),
			                        (bool)stream.ReceiveNext(),
			                        (bool)stream.ReceiveNext());
		}
	}

	public void DidWarp()
	{
		if(PhotonNetwork.isMasterClient && mWarpNetworkInterface !=null)
		{
			mWarpNetworkInterface.DidWarp();
		}
	}

	[RPC]
	public void SetupRigidbodyView(int viewID)
	{
		mRigidbodyView = gameObject.AddComponent<PhotonView>();
		mWarpNetworkInterface = gameObject.AddComponent<WarpPlayerNetworkInterface>();
		mRigidbodyView.observed = mWarpNetworkInterface;
		mRigidbodyView.viewID = viewID;
		mRigidbodyView.synchronization = ViewSynchronization.UnreliableOnChange;
	}

	void Update()
	{
		if(mController!=null)
			mController.Update();
	}

}


