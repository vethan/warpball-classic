﻿using UnityEngine;
using System.Collections;
using Fabric;

public class WarpPlayer : Photon.MonoBehaviour 
{
	public const float RESET_EFFECT_TIME = 0.25f;
	public const float BLAST_INCREASE = 00.0f;

	public const float BLAST_SHAKE_AMOUNT = 0.3f;

	public static float blastChargeRate = 50.0f;
	public static float warpDitanceMultiplier = 1.5f;
	public static float GameSpeed = 2.5f;

	public bool properTest = false;
	Renderer mRenderer;
	Collider2D mCollider;
	float mRadius = 1;
	PlayerInputController inputController = null;
	Ball[] mBalls;

	float reappearTime = 0;
	int mIndex;
	float mBlastCharge = 100;
	WarpPlayerInputNetworkInterface mNet = null;
	ParticleSystem mBlastReady;
	Color mColor = Color.white;
	MaterialPropertyBlock mPropertyBlock = new MaterialPropertyBlock();
	public Color color
	{
		get
		{
			return mColor;
		}
	}

	public void SetNetworkInterface(WarpPlayerInputNetworkInterface inter)
	{
		mNet = inter;
	}

	// Use this for initialization
	void Awake()
	{
		mBalls = FindObjectsOfType<Ball>();

		mRenderer = transform.GetChild(0).GetComponentInChildren<Renderer>();
		mBlastReady = particleSystem;
		photonView.RPC("SetBlastReadyEffect",PhotonTargets.All,true);
		mCollider = collider2D;
	}


	public void SetColor(Color c)
	{
		mColor = c;
		mPropertyBlock.Clear();
		mPropertyBlock.AddColor("_MainColor",mColor);
		mRenderer.SetPropertyBlock(mPropertyBlock);
		mBlastReady.startColor = c;

		if(photonView.isMine)
		{
			photonView.RPC("SetColorRPC",PhotonTargets.OthersBuffered,c.r,c.g,c.b);
		}

	}

	[RPC]
	public void SetColorRPC(float r, float g, float b)
	{
		mColor = new Color(r,g,b);
		mPropertyBlock.Clear();
		mPropertyBlock.AddColor("_MainColor",mColor);
		mRenderer.SetPropertyBlock(mPropertyBlock);
	}

	public void SetInput(PlayerInputController c, bool settingRemoteControl = false)
	{
		if(!settingRemoteControl && (mNet!=null && !mNet.isMine))
			return;
		inputController = c;
		c.OnWarpButtonPressed += () => 
		{
			PlayerControlStatus state = inputController.GetControlStatus();
			if(state.rightStick.magnitude > 0.05)
			{
				Warp(state.rightStick.normalized,state.rightStick.magnitude*warpDitanceMultiplier);
			}
			else
			{
				Warp(state.leftStick.normalized,state.leftStick.magnitude*warpDitanceMultiplier);
			}
		};

		c.OnBlastButtonPressed += UseBlast;
	}

	[RPC]
	public void SetIndex (int i)
	{
		PlayerHUDController.SetPlayerColor(i,mColor);
		PlayerHUDController.SetPlayerCharge(i,(int)mBlastCharge);
		mIndex = i;
		if(photonView.isMine)
		{
			photonView.RPC("SetIndex",PhotonTargets.OthersBuffered,i);
		}
	}

	public void IncreaseBlastCharge()
	{
		mBlastCharge = Mathf.Min(100,mBlastCharge+ BLAST_INCREASE);
		PlayerHUDController.SetPlayerCharge(mIndex,(int)mBlastCharge);
	}

	void UseBlast()
	{
		if(mBlastCharge >= 100)
		{
			photonView.RPC("SetBlastReadyEffect",PhotonTargets.All,false);
			if(mBalls!=null)
			{
				foreach(Ball b in mBalls)
				{
					b.ApplyExplosion(this);
				}
			}
			mBlastCharge = 0;
		}
	}

	void Start () 
	{
		mRadius = mRenderer.bounds.size.x * 0.5f;
	}

	[RPC]
	public void SetBlastReadyEffect(bool enabled)
	{
		if(enabled)
		{
			mBlastReady.Play();
		}
		else
		{
			mBlastReady.Stop();
			mBlastReady.Clear();
			BlastParticlePool.CreateBlastEffect(transform.position, mColor);
			EventManager.Instance.PostEvent("BlastSound");
			ShakeCamera.Shake (BLAST_SHAKE_AMOUNT);
			PlayerHUDController.SetPlayerCharge(mIndex,(int)mBlastCharge);
		}			
	}

	[RPC]
	public void ResetPosition(Vector3 position)
	{
		mRenderer.enabled = false;
		TeleportParticlePool.CreateSlowTeleportEffect(transform.position,position,mColor);
		transform.position = position;
		reappearTime  = Time.time + RESET_EFFECT_TIME;
	}

	void Warp(Vector2 direction, float distance)
	{
		Warp(new Vector3(direction.x,direction.y),distance);
	}

	bool FindDistanceWarpPoint(Vector3 direction, float distance, out Vector2 newEnd)
	{
		float newDistance = distance -0.01f;
		if(newDistance > 0)
		{
			direction.z = 0;
			Vector3 startPos = transform.position;
			Vector3 endPos = startPos + direction  * newDistance;
			newEnd = new Vector2(endPos.x,endPos.y);
			Collider2D[] colliders = Physics2D.OverlapCircleAll(newEnd,mRadius);
			
			if(colliders.Length > 0)
			{
				
				foreach(Collider2D coll in colliders)
				{
					if(coll != mCollider && !coll.isTrigger)
					{
						return FindDistanceWarpPoint(direction,newDistance, out newEnd);
					}
				}
			}
			return true;
		}
		else
		{
			newEnd = Vector2.zero;
			return false;
		}
	}

	bool FindRotationWarpPoint(Vector3 direction, float distance, float rotationAmount, out Vector2 newEnd)
	{
		float newRotation = rotationAmount > 0 ? -rotationAmount : (-rotationAmount) + 1;
		if(Mathf.Abs(newRotation) < 60)
		{
			direction.z = 0;
			Vector3 startPos = transform.position;
			Vector3 endPos = startPos + Quaternion.Euler(0,0,newRotation) * direction  * distance;
			newEnd = new Vector2(endPos.x,endPos.y);
			Collider2D[] colliders = Physics2D.OverlapCircleAll(newEnd,mRadius);
			
			if(colliders.Length > 0)
			{
				
				foreach(Collider2D coll in colliders)
				{
					if(coll != mCollider && !coll.isTrigger)
					{
						return FindRotationWarpPoint(direction,distance,newRotation,out newEnd);
					}
				}
			}
			return true;
		}
		else
		{
			newEnd = Vector2.zero;
			return false;
		}
	}

	void Warp(Vector3 direction, float distance)
	{
		direction.z = 0;
		Vector3 startPos = transform.position;
		//Vector3 endPos = startPos + direction * distance;
		Vector3 endPos = startPos +   direction  * distance;
		Vector2 endPos2D = new Vector2(endPos.x,endPos.y);
		Collider2D[] colliders = Physics2D.OverlapCircleAll(endPos2D,mRadius);

		if(colliders.Length > 0)
		{

			foreach(Collider2D coll in colliders)
			{
				if(coll != mCollider && !coll.isTrigger)
				{
					Vector2 distancePoint;
					Vector2 rotationPoint;
					bool distanceOK = FindDistanceWarpPoint(direction,distance,out distancePoint);
					bool rotationOK = FindRotationWarpPoint(direction,distance,0,out rotationPoint);
					if(distanceOK && !rotationOK)
					{
						endPos2D = distancePoint;
					}
					else if(rotationOK && ! distanceOK)
					{
						endPos2D = rotationPoint;
					}
					else if(rotationOK && distanceOK)
					{
						if((endPos2D-rotationPoint).magnitude >= (endPos2D-distancePoint).magnitude)
						{
							endPos2D = distancePoint;
						}
						else
						{
							endPos2D = rotationPoint;
						}
					}
					else
					{
						return;
					}
				}
			}
		}
		TeleportParticlePool.CreateTeleportEffect(startPos,new Vector3(endPos2D.x,endPos2D.y,endPos.z), mColor);
		transform.position = new Vector3(endPos2D.x,endPos2D.y,endPos.z);
		if((startPos-endPos).magnitude>0.2f)
		{
			EventManager.Instance.PostEvent("WarpSound");
		}
		if(PhotonNetwork.isMasterClient)
		{
			mNet.DidWarp();
		}
	}

	void ProcessInput (PlayerControlStatus controlStatus)
	{
		rigidbody2D.velocity = controlStatus.leftStick*GameSpeed;
	}

	// Update is called once per frame
	void Update () {

		if(mRenderer.enabled == false && reappearTime < Time.time)
		{
			mRenderer.enabled = true;
		}

		if(!MatchController.isGameRunning)
		{
			rigidbody2D.velocity=Vector2.zero;
			return;
		}

		if(PhotonNetwork.isMasterClient && mBlastCharge < 100)
		{
			mBlastCharge = Mathf.Min(100,mBlastCharge+ blastChargeRate*Time.deltaTime);
			PlayerHUDController.SetPlayerCharge(mIndex,(int)mBlastCharge);
			if(mBlastCharge == 100)
			{
				photonView.RPC("SetBlastReadyEffect",PhotonTargets.All,true);
			}
		}

		if(PhotonNetwork.isMasterClient/* || (mNet != null && mNet.isMine)*/)
		{
			inputController.Update();
			ProcessInput(inputController.GetControlStatus());
		}
	}

	public PlayerControlStatus controllerState
	{
		get
		{
			if(inputController != null)
				return inputController.GetControlStatus();
			else
				return new PlayerControlStatus();
		}
	}
}
