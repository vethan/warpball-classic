﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class XInputPlayerController : PlayerInputController 
{
	PlayerIndex mControllerIndex;
	const float TRIGGER_SENSITIVITY = 0.05f;
	bool fire1WasDown = false;
	bool fire2WasDown = false;
	public XInputPlayerController(int controllerIndex)
	{
		GamePadState testState = GamePad.GetState((PlayerIndex)controllerIndex);
		if(testState.IsConnected == false)
		{
			throw new System.Exception("Controller wasn't connected");
		}
		mControllerIndex = (PlayerIndex)controllerIndex;
	}

	public override void Update()
	{
		GamePadState padState = GamePad.GetState(mControllerIndex);


		if(padState.Triggers.Right > TRIGGER_SENSITIVITY && !fire1WasDown)
		{
			OnWarpButtonPressed();
		}
		fire1WasDown = padState.Triggers.Right > TRIGGER_SENSITIVITY;

		if(padState.Buttons.A == ButtonState.Pressed && !fire2WasDown)
		{
			OnBlastButtonPressed();
		}
		fire2WasDown = padState.Buttons.A == ButtonState.Pressed;
	}
	
	public override PlayerControlStatus GetControlStatus()
	{
		GamePadState padState = GamePad.GetState(mControllerIndex);
		Vector2 stickOne = new Vector2(padState.ThumbSticks.Left.X,padState.ThumbSticks.Left.Y);
		
		Vector2 stickTwo = new Vector2(padState.ThumbSticks.Right.X,padState.ThumbSticks.Right.Y);
		PlayerControlStatus newState = new PlayerControlStatus(stickOne,stickTwo,
		                                                       padState.Triggers.Right > TRIGGER_SENSITIVITY,
		                                                       padState.Buttons.A == ButtonState.Pressed);
		
		return newState;
	}
}
