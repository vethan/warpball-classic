using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class NetworkPlayerController : PlayerInputController 
{
	PlayerControlStatus mPlayerControlState;
	bool mWarpWasPressed = false;
	bool mBlastWasPressed = false;

	public NetworkPlayerController()
	{
		mPlayerControlState = new PlayerControlStatus();
	}

	public void SetNewState(Vector2 leftStickValue,Vector2 rightStickValue, bool warp, bool blast)
	{
		mPlayerControlState = new PlayerControlStatus(leftStickValue, rightStickValue, warp,blast);
	}

	public override void Update()
	{
		if(mPlayerControlState.warpButton && !mWarpWasPressed)
		{
			OnWarpButtonPressed();
		}
		mWarpWasPressed = mPlayerControlState.warpButton;

		if(mPlayerControlState.blastButton && !mBlastWasPressed)
		{
			OnBlastButtonPressed();
		}
		mBlastWasPressed = mPlayerControlState.blastButton;
	}
	
	public override PlayerControlStatus GetControlStatus()
	{
		return mPlayerControlState;
	}
}
