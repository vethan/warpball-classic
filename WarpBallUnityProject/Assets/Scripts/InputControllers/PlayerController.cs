using System;
using UnityEngine;

public class PlayerControlStatus
{
	public PlayerControlStatus()
	{

	}

	public PlayerControlStatus(Vector2 LeftStick, Vector2 RightStick, bool WarpButton, bool BlastButton)
	{
		leftStick = LeftStick;
		rightStick = RightStick;
		warpButton = WarpButton;
		blastButton = BlastButton;
	}

	public Vector2 leftStick;
	public Vector2 rightStick;
	public bool warpButton;
	public bool blastButton;
}

public abstract class PlayerInputController
{
	public abstract PlayerControlStatus GetControlStatus();
	public abstract void Update();

	public Action OnWarpButtonPressed = () => {};
	public Action OnBlastButtonPressed = () => {};
}


public class UnityPlayerInputContoller : PlayerInputController
{

	public override void Update()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			OnWarpButtonPressed();
		}

		if(Input.GetButtonDown("Fire2"))
		{
			OnBlastButtonPressed();
		}
	}

	public override PlayerControlStatus GetControlStatus()
	{
		Vector2 stickOne = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));

		Vector2 stickTwo = new Vector2(Input.GetAxis("Mouse X"),Input.GetAxis("Mouse Y"));
		PlayerControlStatus newState = new PlayerControlStatus(stickTwo,stickOne,Input.GetButton("Fire1"),Input.GetButton("Fire2"));

		return newState;
	}
}