﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIPlayer : MonoBehaviour {
	AIInputController inputController;
	int currentState = 0;
	Rigidbody2D mRigidbody;
	Vector2 homePosition;
	int mTeam = 0;

	List<AIState> possibleStates = new List<AIState>();
	float warpStore = 5;
	float warpCharge = 0;
	const float MAX_WARPS = 5;
	const float WARP_CHARGE_RATE = 0.17f;


	public void Initialise(int team, Vector2 homePos, AIInputController input)
	{
		inputController = input;
		homePosition = homePos;
		mTeam = team;
		mRigidbody = rigidbody2D;
		SetupAIStates();
	}

	void SetupAIStates()
	{
		possibleStates.Add(new AIDefendState(homePosition,mRigidbody));
		possibleStates.Add(new AIDribbleState(homePosition,mRigidbody));
		possibleStates.Add(new AIWanderState(homePosition,mRigidbody));
		currentState = 0;
	}


	void EvaluateStates()
	{
		float[] heuristics = new float[possibleStates.Count];
		int maxIndex = -1;

		for(int i = 0; i < possibleStates.Count; i++)
		{
			heuristics[i] = possibleStates[i].GetHeuristic();
			if(maxIndex == -1 || heuristics[i] > heuristics[maxIndex])
			{
				maxIndex = i;
			}
		}

		if(maxIndex != currentState && heuristics[maxIndex] > heuristics[currentState] + 2)
		{
			currentState = maxIndex;
		}
	}

	// Use this for initialization
	void Start () {
	
	}

	public void OnDrawGizmos() 
	{
		if(Camera.current != Camera.main)
			return;

		Vector2 targetPosition = possibleStates[currentState].GetTargetPosition();
		possibleStates[currentState].DrawGizmos();
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(new Vector3(targetPosition.x,targetPosition.y,0), 0.1f);
	}

	void UpdateWarpAvailability()
	{
		if(warpStore < MAX_WARPS)
		{
			warpCharge += Time.deltaTime;
			if(warpCharge > WARP_CHARGE_RATE)
			{
				warpCharge-=WARP_CHARGE_RATE;
				warpStore++;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		EvaluateStates();
		UpdateWarpAvailability();
		inputController.mControlStatus = possibleStates[currentState].UpdateControlStatus(inputController.mControlStatus,
		                                                                  mTeam);
		if(inputController.mControlStatus.warpButton)
		{
			if(warpStore > 0)
			{
				warpStore--;
				inputController.AttemptToWarp();
			}
		}

		if(inputController.mControlStatus.blastButton)
		{
			inputController.AttemptToBlast();
		}
	}
}
