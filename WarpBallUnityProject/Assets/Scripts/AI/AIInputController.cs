﻿using UnityEngine;
using System.Collections;

public class AIInputController : PlayerInputController {
	const float TIME_BETWEEN_WARPS = 0.1f;
	float nextCanWarp = 0.0f;
	public PlayerControlStatus mControlStatus = new PlayerControlStatus();



	public void AttemptToWarp()
	{
		if(Time.time > nextCanWarp)
		{
			OnWarpButtonPressed();
			nextCanWarp = TIME_BETWEEN_WARPS + Time.time;
		}
	}

	public void AttemptToBlast()
	{
		OnBlastButtonPressed();
	}
	
	public override PlayerControlStatus GetControlStatus()
	{
		return mControlStatus;
	}

	public override void Update()
	{

	}
}
