﻿using UnityEngine;
using System.Collections.Generic;

public class AIController {
	public static Packet UpdateDataPacket = null;
	const int BOUNCE_LOOK_AHEAD = 3;
	Ball mBall;
	Rigidbody2D mBallRigidbody;
	protected Dictionary<Goal,int> mGoalOwners;
	List<Goal> mGoals;

	Vector2 lastBallDirection = Vector2.zero;
	List<BounceInfo> mBallBounces = new List<BounceInfo>();
	public AIController(Ball ball, Dictionary<Goal,int> goalOwners)
	{
		mGoalOwners = goalOwners;
		mBall = ball;
		mBallRigidbody = ball.rigidbody2D;
		mGoals = new List<Goal>(goalOwners.Keys);
	}

	// Use this for initialization
	void Start () {
	
	}

	void PredictBounce(Vector2 position, Vector2 direction, float speed, int bounceCount)
	{
		if(speed <=  0)
			return;

		RaycastHit2D hit = Physics2D.Raycast(position,direction,100,~LayerMask.GetMask("Balls"));
		float distance = (hit.point - position).magnitude;
		float time = distance/speed;

		if(mBallBounces.Count == 0)
		{
			time = time + Time.time;
		}
		else
		{
			time = time + mBallBounces[mBallBounces.Count-1].bounceTime;
		}
		BounceInfo b = new BounceInfo();
		b.bounceTime = time;
		b.bouncePoint = hit.point;
		b.bounceDirection = -2 * (Vector2.Dot(direction,hit.normal)*hit.normal) + direction;
		if(hit.transform == null)
			return;
		if(hit.transform.tag == "Goal")
		{
			b.bounceType = BounceInfo.BounceType.Goal;
			b.bouncedObject = hit.transform.gameObject;
			b.objectTeam = mGoalOwners[b.bouncedObject.GetComponent<Goal>()];
		}
		else if(hit.transform.tag == "Player")
		{
			b.bounceType = BounceInfo.BounceType.Player;
			b.bouncedObject = hit.transform.gameObject;			
		}
		else
		{
			b.bounceType = BounceInfo.BounceType.Environment;
		}
		mBallBounces.Add(b);
		if(bounceCount > BOUNCE_LOOK_AHEAD || b.bounceType == BounceInfo.BounceType.Goal)
		{
			return;
		}
		else
		{
			PredictBounce(b.bouncePoint,b.bounceDirection,speed,bounceCount+1);
		}	
	}

	// Update is called once per frame
	public void Update () 
	{
		//if(lastBallDirection != mBallRigidbody.velocity.normalized)
		{
			mBallBounces.Clear();
			lastBallDirection = mBallRigidbody.velocity.normalized;
			float speed = mBallRigidbody.velocity.magnitude;
			PredictBounce(mBallRigidbody.position,lastBallDirection,speed,0);
			UpdateDataPacket = new Packet(mBallRigidbody.position,lastBallDirection,speed,mBallBounces);
		}
	}

	public struct BounceInfo
	{
		public enum BounceType {Player, Goal, Environment};
		public float bounceTime;
		public int objectTeam;
		public BounceType bounceType;
		public GameObject bouncedObject;
		public Vector2 bouncePoint;
		public Vector2 bounceDirection;
	}

	public class Packet
	{
		public List<BounceInfo> BallBounces;
		public Vector2 ballPosition;
		public Vector2 ballDirection;
		public float ballSpeed;

		public Packet(Vector2 ballPos, Vector2 ballDir, float ballSpd, List<BounceInfo> bounces)
		{
			ballPosition = ballPos;
			ballDirection = ballDir;
			ballSpeed = ballSpd;
			BallBounces = bounces;
		}
	}
}

