﻿using UnityEngine;
using System.Collections;

public class AIAttackState : AIState
{
	const float RETARGET_LIKELYHOOD = 0.1f;
	const float STICK_MOVEMENT_MAX = 5;
	const float WARP_THRESHOLD = 1.25f*1.25f;
	float mMoveRadius = 1f;
	Vector2 mHomePosition;

	Vector2 targetPosition;
	Rigidbody2D mSelf;
	Rigidbody2D opponent;
	Transform myGoal;
	Transform opponentGoal;

	public AIAttackState(Vector2 homePosition, Rigidbody2D self)
	{
		mHomePosition = homePosition;
		mSelf = self;
		WarpPlayer[] players = GameObject.FindObjectsOfType<WarpPlayer>();
		for(int i = 0 ; i < players.Length; i++)
		{
			if(players[i].gameObject.GetComponent<AIPlayer>() == null)
			{
				opponent = players[i].rigidbody2D;
				break;
			}
		}

		Goal[] goals = GameObject.FindObjectsOfType<Goal>();
		for(int i = 0 ; i < goals.Length; i++)
		{
			if(Mathf.Sign(goals[i].transform.position.x) == Mathf.Sign(mHomePosition.x))
			{
				myGoal = goals[i].transform;
			}
			else
			{
				opponentGoal = goals[i].transform;
			}
		}		
	}

	public void DrawGizmos()
	{

	}

	public float GetHeuristic()
	{
		return 1;
	}

	public Vector2 GetTargetPosition()
	{
		return targetPosition;
	}

	void NewTargetPosition()
	{
		Vector2 randVec = Random.insideUnitCircle * mMoveRadius;
		targetPosition = mHomePosition + randVec;
	}

	public PlayerControlStatus UpdateControlStatus(PlayerControlStatus currentState, int team)
	{
		float randVal = Random.value;
		AIController.Packet dataPack = AIController.UpdateDataPacket;
		Vector2 ballPosition;
		if(dataPack !=null) 
		{
			Vector2 ballToGoal = (Vector2)opponentGoal.transform.position - dataPack.ballPosition;
			targetPosition = dataPack.ballPosition - (ballToGoal.normalized)*0.1f;
			ballPosition = dataPack.ballPosition;
		}
		else
		{
			targetPosition = mHomePosition;
			ballPosition = targetPosition;
		}
		
		Vector2 movedir = (targetPosition-mSelf.position).normalized;

		currentState.leftStick = Vector2.MoveTowards(currentState.leftStick,
		                                             movedir,
		                                             Time.deltaTime* STICK_MOVEMENT_MAX);

		if((targetPosition-mSelf.position).sqrMagnitude > WARP_THRESHOLD)
		{
			currentState.rightStick = movedir;
			currentState.warpButton = true;
		}
		else
		{
			currentState.warpButton = false;
		}


		return currentState;
	}
}