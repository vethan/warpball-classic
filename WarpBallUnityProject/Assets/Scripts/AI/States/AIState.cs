﻿using UnityEngine;
using System.Collections;

public interface AIState
{

	PlayerControlStatus UpdateControlStatus(PlayerControlStatus currentState, int team);
	Vector2 GetTargetPosition();
	void DrawGizmos();
	float GetHeuristic();
}

public class AIWanderState : AIState
{
	const float RETARGET_LIKELYHOOD = 0.1f;
	const float STICK_MOVEMENT_MAX = 5;
	const float WARP_THRESHOLD = 2*2;
	float mMoveRadius = 1f;
	Vector2 mHomePosition;
	Rigidbody2D mSelf;
	Vector2 targetPosition;

	public AIWanderState(Vector2 homePosition, Rigidbody2D self)
	{
		mSelf = self;
		mHomePosition = homePosition;
	}

	public void DrawGizmos()
	{

	}

	public Vector2 GetTargetPosition()
	{
		return targetPosition;
	}

	void NewTargetPosition()
	{
		Vector2 randVec = Random.insideUnitCircle * mMoveRadius;
		targetPosition = mHomePosition + randVec;
	}

	public float GetHeuristic()
	{
		return 1;
	}

	public PlayerControlStatus UpdateControlStatus(PlayerControlStatus currentState, int team)
	{
		float randVal = Random.value;
		if(mSelf.position == targetPosition || Random.value < RETARGET_LIKELYHOOD)
		{
			NewTargetPosition();
		}
		Vector2 movedir = (targetPosition-mSelf.position).normalized;

		currentState.leftStick = Vector2.MoveTowards(currentState.leftStick,
		                                             movedir,
		                                             Time.deltaTime* STICK_MOVEMENT_MAX);
		currentState.rightStick = movedir;


		currentState.warpButton = (targetPosition-mSelf.position).sqrMagnitude > WARP_THRESHOLD;


		return currentState;
	}
}