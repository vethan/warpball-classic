﻿using UnityEngine;
using System.Collections;

public class AIDefendState : AIState
{
	const float RETARGET_LIKELYHOOD = 0.1f;
	const float STICK_MOVEMENT_MAX = 5;
	const float WARP_THRESHOLD = 2*2;
	float mMoveRadius = 1f;
	Vector2 mHomePosition;

	Vector2 targetPosition;
	float timeWillSave = 0;
	Rigidbody2D opponent;
	Rigidbody2D mSelf;
	Transform myGoal;
	Transform opponentGoal;

	public AIDefendState(Vector2 homePosition, Rigidbody2D self)
	{
		mHomePosition = homePosition;
		mSelf = self;
		WarpPlayer[] players = GameObject.FindObjectsOfType<WarpPlayer>();
		for(int i = 0 ; i < players.Length; i++)
		{
			if(players[i].rigidbody2D != self)
			{
				opponent = players[i].rigidbody2D;
				break;
			}
		}

		Goal[] goals = GameObject.FindObjectsOfType<Goal>();
		for(int i = 0 ; i < goals.Length; i++)
		{
			if(Mathf.Sign(goals[i].transform.position.x) == Mathf.Sign(mHomePosition.x))
			{
				myGoal = goals[i].transform;
			}
			else
			{
				opponentGoal = goals[i].transform;
			}
		}
		
	}

	public Vector2 GetTargetPosition()
	{
		return targetPosition;
	}

	public float GetHeuristic()
	{
		AIController.Packet dataPack = AIController.UpdateDataPacket;
		if(dataPack ==null || dataPack.BallBounces.Count < 1) 
			return 1;

		float ballInvSqDistanceFromGoal = 30.0f/((dataPack.ballPosition - (Vector2)myGoal.position).sqrMagnitude);
		float ballTowardsGoalAmount = Vector2.Dot(dataPack.ballDirection.normalized,((Vector2)myGoal.position - dataPack.ballPosition).normalized);
		float result = ballInvSqDistanceFromGoal*ballTowardsGoalAmount;
		if(dataPack.BallBounces[dataPack.BallBounces.Count-1].bouncedObject == myGoal.gameObject)
		{
			result +=5;
			result *= 5-dataPack.BallBounces.Count;
		}
		opponentAim = dataPack.ballPosition - opponent.position;
		RaycastHit2D hit = Physics2D.Raycast(opponent.position+(opponentAim.normalized*0.5f),opponentAim.normalized,100,~LayerMask.GetMask("Balls"));
		if(hit.transform == myGoal)
		{
			result +=5;
			result *= 3;
		}
		
		//Debug.Log("Defend State Heuristic: " + result);
		
		return result;
	}

	public void DrawGizmos()
	{
		Gizmos.DrawRay(opponent.position,opponentAim);
	}
	Vector2 opponentAim;
	public Vector2 FindRestingPoint(Vector2 ballPosition)
	{
		opponentAim = ballPosition - opponent.position;
		RaycastHit2D hit = Physics2D.Raycast(opponent.position+(opponentAim.normalized*0.5f),opponentAim.normalized,100,~LayerMask.GetMask("Balls"));
		if(hit.transform == myGoal)
		{
			return hit.point;
		}
		return mHomePosition;
	}

	public PlayerControlStatus UpdateControlStatus(PlayerControlStatus currentState, int team)
	{
		AIController.Packet dataPack = AIController.UpdateDataPacket;
		if(dataPack !=null && dataPack.BallBounces.Count > 0) 
		{
			AIController.BounceInfo finalBounce = dataPack.BallBounces[dataPack.BallBounces.Count-1];
			if(finalBounce.bouncedObject == myGoal.gameObject)
			{
				targetPosition = finalBounce.bouncePoint;
				timeWillSave = finalBounce.bounceTime;
			}
			else
			{
				if(Time.time > timeWillSave)
					targetPosition = FindRestingPoint(dataPack.ballPosition);
			}
			Vector2 balldir = dataPack.ballPosition-mSelf.position;
			currentState.blastButton = balldir.sqrMagnitude < 0.25f && Mathf.Sign(balldir.x) != Mathf.Sign(mHomePosition.x) && Mathf.Sign(dataPack.ballDirection.x)!= Mathf.Sign(mHomePosition.x);
		}
		else
		{
			targetPosition = mHomePosition;
		}

		Vector2 movedir = (targetPosition-mSelf.position)*2;
		if(movedir.magnitude > 1)
			movedir.Normalize();

		currentState.leftStick = Vector2.MoveTowards(currentState.leftStick,
		                                             movedir,
		                                             Time.deltaTime* STICK_MOVEMENT_MAX);


		currentState.rightStick = Vector2.MoveTowards(currentState.rightStick,
		                                              movedir,
		                                              Time.deltaTime* STICK_MOVEMENT_MAX);



		currentState.warpButton =  Mathf.Sign(movedir.x) == Mathf.Sign(mHomePosition.x) &&(targetPosition-mSelf.position).sqrMagnitude > WARP_THRESHOLD;



		return currentState;
	}
}