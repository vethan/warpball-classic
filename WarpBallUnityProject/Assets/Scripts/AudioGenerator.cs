﻿using UnityEngine;
using System.Collections;

public class AudioGenerator : MonoBehaviour {

	public GameObject fabricManager;

	// Use this for initialization
	void Awake () 
	{
		if(Fabric.FabricManager.Instance == null)
		{
			GameObject.Instantiate(fabricManager);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
