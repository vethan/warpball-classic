﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;
public class PlayerInfo
{
	public PlayerInfo(PlayerIndex idx, int team, int teamSlot, Color color)
	{
		index = idx;
		mTeam = team;
		mTeamSlot = teamSlot;
		mColor = color;
	}

	public PlayerInfo(string playerName, int team, int teamSlot, Color color)
	{
		mPlayerName = playerName;
		mOnlinePlayer = true;
		mTeam = team;
		mTeamSlot = teamSlot;
		mColor = color;
	}

	public PlayerIndex index;
	string mPlayerName;
	int mTeam = 0;
	int mTeamSlot = 0;
	bool mOnlinePlayer = false;
	Color mColor;

	public string name
	{
		get
		{
			return mPlayerName;
		}
		set
		{
			mPlayerName = value;
		}
	}

	public Color color
	{
		get
		{
			return mColor;
		}
	}

	public int teamSlot
	{
		get
		{
			return mTeamSlot;
		}
	}


	public int team
	{
		get
		{
			return mTeam;
		}
	}

	public bool online
	{
		get
		{
			return mOnlinePlayer;
		}
	}


}

public class MatchControlPacket : MonoBehaviour {

	PlayerInfo[] playerInfos = new PlayerInfo[4];
	MatchSettingsMenu.GameType gameType;
	int gameLimitValue;

	int[] teamCounters = new int[2];
	// Use this for initialization
	void Start () {

	}

	public void SetUpCoreData(MatchSettingsMenu.GameType type, int value)
	{
		teamCounters[0] = 0;
		teamCounters[1] = 0;
		DontDestroyOnLoad(gameObject);
		gameType = type;
		gameLimitValue = value;
	}

	public void SetPlayer(int index, PlayerIndex controllerIndex, int team)
	{
		playerInfos[index] = new PlayerInfo(controllerIndex, team, teamCounters[team], WarpBall.PlayerColors[index]);
		teamCounters[team]++;
	}

	public void SetPlayer(int index, string playerName, int team)
	{
		playerInfos[index] = new PlayerInfo(playerName, team, teamCounters[team], WarpBall.PlayerColors[index]);
		teamCounters[team]++;
	}

	public PlayerInfo GetPlayer(int index)
	{
		return playerInfos[index];
	}

	public MatchSettingsMenu.GameType GetGameType()
	{
		return gameType;
	}

	public int GetGameLimit()
	{
		return gameLimitValue;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
