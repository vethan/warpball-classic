using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OriginalMatchController : MatchController {

	public List<WarpPlayer> team1Players = new List<WarpPlayer>();
	public List<WarpPlayer> team2Players = new List<WarpPlayer>();
	public List<Ball> balls = new List<Ball>();
	public Goal rightGoal = null;
	public Goal leftGoal = null;

	const float ySpawnSplit = 1.8f;
	const float xSpawnSplit = 3.0f;

	Vector2 ballDirection = Vector2.zero;
	MatchSettingsMenu.GameType mGameType = MatchSettingsMenu.GameType.TimedGame;

	int mLimit = 5;

	float mTimePassed = 0.0f;

	// Use this for initialization

	void Start () 
	{
		for(int i = 0; i < 4; i++)
		{
			PlayerHUDController.DisableHUD(i);
		}

		if(!PhotonNetwork.inRoom)
		{
			PhotonNetwork.offlineMode = true;
			PhotonNetwork.CreateRoom(null);
		}

		MatchControlPacket matchPacket = FindObjectOfType<MatchControlPacket>();
		if(PhotonNetwork.isMasterClient)
		{
			balls.Add(PhotonNetwork.Instantiate("Ball",Vector3.zero,Quaternion.identity,0).GetComponent<Ball>());
			OnScore += SetBallVector;
			OnScore += CheckScore;
		}

		if(matchPacket!=null)
		{
			for(int i = 0; i < 4; i++)
			{
				PlayerInfo pInfo = matchPacket.GetPlayer(i);
				if(pInfo!=null)
				{
					if(!pInfo.online)
					{
						WarpPlayer newPlayer = PhotonNetwork.Instantiate("Player",Vector3.zero,Quaternion.identity,0).GetComponent<WarpPlayer>();
						newPlayer.SetColor(pInfo.color);
						newPlayer.SetInput(new XInputPlayerController((int)pInfo.index));
						newPlayer.SetIndex(pInfo.team + (pInfo.teamSlot*2));
						photonView.RPC("AddPlayerToLists",PhotonTargets.MasterClient,newPlayer.photonView.viewID,pInfo.team);
					}

				}
			}

			mGameType = matchPacket.GetGameType();
			mLimit = matchPacket.GetGameLimit();

			if(mGameType == MatchSettingsMenu.GameType.TimedGame)
			{
				mLimit*=60;
			}

		}

		goalOwners[rightGoal] = 0;
		goalOwners[leftGoal] = 1;

		scores = new int[2];
		scores[0] = 0;
		scores[1] = 0;

		if(PhotonNetwork.isMasterClient)
		{
			ResetPositions();
		}


		//mGameRunning = true;
	}

	[RPC]
	void AddPlayerToLists(int viewID, int team)
	{
		PhotonView p = PhotonView.Find(viewID);
		if(p==null)
			return;

		WarpPlayer newPlayer = p.GetComponent<WarpPlayer>();

		if(team == 0)
		{
			team1Players.Add(newPlayer);
		}
		else if(team == 1)
		{
			team2Players.Add(newPlayer);
		}
	}

	public override string GetElapsedTime ()
	{
		if(mGameRunning == false)
		{
			return "  ";
		}

		int timeInSecs = mGameType == MatchSettingsMenu.GameType.TimedGame ? mLimit - (int)mTimePassed : (int)mTimePassed;
		if(timeInSecs >= 0)
		{
			return (timeInSecs/60) + ":" + (timeInSecs%60).ToString("00");
		}
		else
		{
			return "Golden Goal!";
		}

	}

	public void SetBallVector(int teamScored)
	{
		ballDirection =  teamScored == 0 ? new Vector2(1,0) : new Vector2(-1,0);
	}

	public void CheckScore(int teamScored)
	{
		if(mGameType == MatchSettingsMenu.GameType.ScoreGame && scores[teamScored] >= mLimit)
		{
			photonView.RPC("EndGame",PhotonTargets.AllBuffered);	
		}
	}

	public override void ResetPositions()
	{
		if(!PhotonNetwork.isMasterClient)
			return;

		if(team1Players.Count == 1)
		{
			team1Players[0].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(-xSpawnSplit,0,0));
		}
		else if(team1Players.Count == 2)
		{
			team1Players[0].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(-xSpawnSplit,ySpawnSplit,0));
			team1Players[1].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(-xSpawnSplit,-ySpawnSplit,0));
		}

		if(team2Players.Count == 1)
		{
			team2Players[0].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(xSpawnSplit,0,0));
		}
		else if(team2Players.Count == 2)
		{
			team2Players[0].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(xSpawnSplit,ySpawnSplit,0));
			team2Players[1].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(xSpawnSplit,-ySpawnSplit,0));
		}

		if(balls.Count == 1)
		{
			balls[0].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(0,0,0),ballDirection);
		}
		else if(balls.Count == 2)
		{
			balls[0].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(0,ySpawnSplit,0),ballDirection);
			balls[1].photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(0,-ySpawnSplit,0),ballDirection);
		}

	}

	// Update is called once per frame
	void Update () 
	{
		if(mGameRunning && PhotonNetwork.isMasterClient)
		{
			mTimePassed += Time.deltaTime;
			if(mGameType == MatchSettingsMenu.GameType.TimedGame && mTimePassed > mLimit && GetTeamScore(0) != GetTeamScore(1))
			{
				photonView.RPC("EndGame",PhotonTargets.AllBuffered);
			}
		}

		if(Input.GetKeyDown(KeyCode.Q))
		{
			if(PhotonNetwork.offlineMode == true)
			{
				PhotonNetwork.LeaveRoom();
				PhotonNetwork.offlineMode = false;
			}
			else if(PhotonNetwork.inRoom == true)
			{
				PhotonNetwork.LeaveRoom();
				PhotonNetwork.Disconnect();
			}
			PhotonNetwork.LoadLevel("MainMenu");
		}
	}
}
