﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Fabric;

public abstract class MatchController : Photon.MonoBehaviour
{
	public static bool isRematch = false;
	protected static MatchController instance;
	public UIPanel instructionsScreen;
	protected int[] scores = new int[2];
	protected Dictionary<Goal,int> goalOwners = new Dictionary<Goal, int>();

	protected System.Action<int> OnScore = (int team) => {};

	protected bool mGameRunning = false;

	public static bool isGameRunning
	{
		get
		{
			if(instance == null)
				return false;
			return instance.mGameRunning;
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{

		if (stream.isWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(scores.Length);
			for(int i = 0; i < scores.Length; i++)
			{
				stream.SendNext(scores[i]);
			}
			stream.SendNext(mGameRunning);
		}
		else
		{
			// Network player, receive data
			int scoreLength = (int)stream.ReceiveNext();
			if(scores.Length != scoreLength)
			{
				scores = new int[scoreLength];
			}
			for(int i = 0; i < scores.Length; i++)
			{
				scores[i] = (int)stream.ReceiveNext();
			}
			mGameRunning = (bool)stream.ReceiveNext();
		}
	}

	public static void StartGame()
	{
		if(instance!=null)
		{
			instance.mGameRunning = true;
		}
	}

	public void Awake()
	{
		EventManager.Instance.PostEvent("PlayGameBGM");

		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this.gameObject);
			return;
		}

		if(isRematch)
		{
			Destroy(instructionsScreen.gameObject);
			StartGame();
		}
	}
	
	public void OnDestroy()
	{
		if(instance == this)
		{
			instance = null;
		}
	}


	public static void ResetGamePositions()
	{
		if(instance == null)
			return;

		instance.ResetPositions();
	}

	public static void Score(Goal goalScoredIn)
	{
		if(instance!=null)
		{
			instance.ScoreGoal(goalScoredIn);
			ShakeCamera.Shake(0.4f);
		}
	}

	public static string GetTime()
	{
		if(instance == null)
		{
			print("noinstance");
			return "0:00";
		}
		
		return instance.GetElapsedTime();
	}

	public static int GetWinningTeam()
	{
		if(instance == null)
		{
			print("noinstance");
			return 0;
		}
		
		return instance.WinningTeam();
	}

	public static int GetTeamScore(int team)
	{
		if(instance == null)
		{
			print("noinstance");
			return 0;
		}

		return instance.GetScore(team);
	}

	int GetScore(int team)
	{
		return scores[team];
	}

	int WinningTeam()
	{
		int currentWinningScore = -1;
		int currentWinningTeam = -1;
		for(int i = 0; i < scores.Length; i++)
		{
            if (scores[i] > currentWinningScore)
			{
				currentWinningTeam = i;
				currentWinningScore = scores[i];
			}
		}
		return currentWinningTeam;
	}

	void ScoreGoal(Goal goalScoredIn)
	{
		if(!goalOwners.ContainsKey(goalScoredIn))
		{
			print("SomethingWentWrong");
			return;
		}

		int teamThatScored = goalOwners[goalScoredIn];
		EventManager.Instance.PostEvent("GoalSound");
		scores[teamThatScored] = scores[teamThatScored]+1;
		//TODO: stuffwiththat


		OnScore(teamThatScored);
		ResetPositions();
	}

	[RPC]
	public void EndGame()
	{
		mGameRunning = false;
		GameOverPanelController.ShowGameOverScreen(GetWinningTeam()+1);
	}

	public abstract void ResetPositions();
	public abstract string GetElapsedTime();

}
