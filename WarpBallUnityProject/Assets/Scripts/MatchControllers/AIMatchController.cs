﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIMatchController : MatchController {
	public WarpPlayer team1Player;
	public WarpPlayer team2Player;
	public Ball ball;
	public Goal rightGoal = null;
	public Goal leftGoal = null;
	
	const float ySpawnSplit = 1.8f;
	const float xSpawnSplit = 3.0f;
	
	Vector2 ballDirection = Vector2.zero;
	MatchSettingsMenu.GameType mGameType = MatchSettingsMenu.GameType.TimedGame;
	
	int mLimit = 5*60; //Time Limit in seconds
	
	float mTimePassed = 0.0f;
	AIController aiController;
	// Use this for initialization
	
	void Start () 
	{

		for(int i = 0; i < 4; i++)
		{
			PlayerHUDController.DisableHUD(i);
		}

		PhotonNetwork.offlineMode = true;
		PhotonNetwork.CreateRoom(null);

		
		MatchControlPacket matchPacket = FindObjectOfType<MatchControlPacket>();

		ball = (PhotonNetwork.Instantiate("Ball",Vector3.zero,Quaternion.identity,0).GetComponent<Ball>());
		OnScore += SetBallVector;
		OnScore += CheckScore;

		//Setup human player
		team1Player = PhotonNetwork.Instantiate("Player",Vector3.zero,Quaternion.identity,0).GetComponent<WarpPlayer>();
		team1Player.SetColor(WarpBall.PlayerColors[0]);
		team1Player.SetInput(new XInputPlayerController((int)WarpBall.activeController));
		team1Player.SetIndex(0);

		//Setup AI player
		team2Player = PhotonNetwork.Instantiate("Player",Vector3.zero,Quaternion.identity,0).GetComponent<WarpPlayer>();
		AIPlayer aiBrain = team2Player.gameObject.AddComponent<AIPlayer>();
		AIInputController aiInput = new AIInputController();
		aiBrain.Initialise(1,new Vector2(xSpawnSplit,0),aiInput);
		team2Player.SetColor(WarpBall.PlayerColors[1]);
		team2Player.SetInput(aiInput);
		team2Player.SetIndex(1);


		goalOwners[rightGoal] = 0;
		goalOwners[leftGoal] = 1;
		aiController = new AIController(ball,goalOwners);
		scores = new int[2];
		scores[0] = 0;
		scores[1] = 0;
		
		ResetPositions();

		//mGameRunning = true;
	}
	
	public override string GetElapsedTime ()
	{
		if(mGameRunning == false)
		{
			return "  ";
		}
		
		int timeInSecs = mGameType == MatchSettingsMenu.GameType.TimedGame ? mLimit - (int)mTimePassed : (int)mTimePassed;
		if(timeInSecs >= 0)
		{
			return (timeInSecs/60) + ":" + (timeInSecs%60).ToString("00");
		}
		else
		{
			return "Golden Goal!";
		}
		
	}
	
	public void SetBallVector(int teamScored)
	{
		ballDirection =  teamScored == 0 ? new Vector2(1,0) : new Vector2(-1,0);
	}
	
	public void CheckScore(int teamScored)
	{
		if(mGameType == MatchSettingsMenu.GameType.ScoreGame && scores[teamScored] >= mLimit)
		{
			photonView.RPC("EndGame",PhotonTargets.AllBuffered);	
		}
	}
	
	public override void ResetPositions()
	{
		team1Player.photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(-xSpawnSplit,0,0));

		team2Player.photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(xSpawnSplit,0,0));	

		ball.photonView.RPC("ResetPosition",PhotonTargets.All,new Vector3(0,0,0),ballDirection);		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(mGameRunning)
		{
			mTimePassed += Time.deltaTime;
			if(mGameType == MatchSettingsMenu.GameType.TimedGame && mTimePassed > mLimit && GetTeamScore(0) != GetTeamScore(1))
			{
				photonView.RPC("EndGame",PhotonTargets.AllBuffered);
			}
			aiController.Update();
		}
		
		if(Input.GetKeyDown(KeyCode.Q))
		{
			PhotonNetwork.LeaveRoom();
			PhotonNetwork.offlineMode = false;
			PhotonNetwork.LoadLevel("MainMenu");
		}
	}
}
