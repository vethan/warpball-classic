﻿using UnityEngine;
using System.Collections;

public class MatchPacketDestroyer : MonoBehaviour {
	// Use this for initialization
	void Start () {
		MatchControlPacket[] matchPackets = FindObjectsOfType<MatchControlPacket>();
		if(matchPackets != null)
		{
			foreach(MatchControlPacket matchPacket in matchPackets)
			{
				Destroy(matchPacket);
			}
		}
		Destroy(gameObject);
	}
}
