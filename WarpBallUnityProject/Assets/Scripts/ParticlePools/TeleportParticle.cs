﻿using UnityEngine;
using System.Collections;

public class TeleportParticle : MonoBehaviour {

	ParticleSystem mParticleSystem;
	Rigidbody mRigidbody;
	public const float EFFECT_LENGTH = 0.1F;
	float timeToStop = 0;
	bool isPlaying = false;
	// Use this for initialization
	void Awake () 
	{
		mParticleSystem = particleSystem;
		mParticleSystem.Stop();
	}

	public void BeginEffect(Vector3 startPosition, Vector3 endPosition, Color color, float timeToTake)
	{
		particleSystem.enableEmission=false;
		rigidbody2D.WakeUp();
		mParticleSystem.startColor = color;
		transform.position = startPosition;

		Vector3 distance = (endPosition - startPosition);

		rigidbody2D.velocity = new Vector2(distance.x,distance.y)/timeToTake;

		timeToStop = Time.time + timeToTake;
		mParticleSystem.Play();
		isPlaying = true;
	}

	void FixedUpdate()
	{
		if(isPlaying && Time.time > timeToStop)
		{
			mParticleSystem.Stop();
			rigidbody2D.Sleep();
			TeleportParticlePool.ReQueue(this);
			isPlaying = false;
		}
		else if(isPlaying && !mParticleSystem.enableEmission)
		{
			mParticleSystem.enableEmission = true;
		}
	}

	// Update is called once per frame
	void Update () 
	{

	}
}
