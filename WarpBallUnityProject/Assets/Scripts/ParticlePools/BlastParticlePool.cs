﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlastParticlePool : MonoBehaviour {
	static BlastParticlePool instance;
	
	public BlastParticle blastParticleEffectPrefab;
	public Queue<BlastParticle> blastParticles = new Queue<BlastParticle>();
	public int particlesToBeginWith = 10;
	
	public static void CreateBlastEffect(Vector3 position, Color color)
	{
		if(instance == null)
		{
			return;
		}
		
		instance.StartBlastEffect(position, color);
	}
	
	public static void ReQueue(BlastParticle particle)
	{
		if(instance == null)
		{
			Destroy(particle.gameObject);
			return;
		}
		
		instance.blastParticles.Enqueue(particle);
	}
	
	
	void StartBlastEffect(Vector3 position, Color color)
	{
		if(blastParticles.Count == 0)
			EnqueueNewSystem();
		BlastParticle p = blastParticles.Dequeue();
		p.BeginEffect(position, color);
	}
	
	void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	
	void OnDestroy()
	{
		if(instance == this)
		{
			instance = null;
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		for(int i = 0; i< particlesToBeginWith; i++)
		{
			EnqueueNewSystem();
		}
	}
	
	void EnqueueNewSystem()
	{
		blastParticles.Enqueue(((GameObject)GameObject.Instantiate(blastParticleEffectPrefab.gameObject)).GetComponent<BlastParticle>());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}


