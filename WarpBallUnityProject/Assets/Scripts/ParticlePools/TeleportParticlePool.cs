﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TeleportParticlePool : MonoBehaviour {
	static TeleportParticlePool instance;

	public TeleportParticle teleportParticleEffectPrefab;
	public Queue<TeleportParticle> teleportParticles = new Queue<TeleportParticle>();
	public int particlesToBeginWith = 10;

	public static void CreateSlowTeleportEffect (Vector3 startPosition, Vector3 endPosition, Color color)
	{
		if(instance == null)
		{
			return;
		}
		
		instance.StartTeleportEffect(startPosition,endPosition,color, WarpPlayer.RESET_EFFECT_TIME);
	}

	public static void CreateTeleportEffect(Vector3 startPosition, Vector3 endPosition, Color color)
	{
		if(instance == null)
		{
			return;
		}

		instance.StartTeleportEffect(startPosition,endPosition,color);
	}

	public static void ReQueue(TeleportParticle particle)
	{
		if(instance == null)
		{
			Destroy(particle.gameObject);
			return;
		}
		
		instance.teleportParticles.Enqueue(particle);
	}

	void StartTeleportEffect(Vector3 startPosition, Vector3 endPosition, Color color)
	{
		StartTeleportEffect(startPosition, endPosition, color, TeleportParticle.EFFECT_LENGTH);
	}

	void StartTeleportEffect(Vector3 startPosition, Vector3 endPosition, Color color, float timeToTake)
	{
		if(teleportParticles.Count == 0)
			EnqueueNewSystem();

		TeleportParticle p = teleportParticles.Dequeue();
		p.BeginEffect(startPosition,endPosition,color, timeToTake);
	}

	void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	void OnDestroy()
	{
		if(instance == this)
		{
			instance = null;
		}
	}

	// Use this for initialization
	void Start ()
	{
		for(int i = 0; i< particlesToBeginWith; i++)
		{
			EnqueueNewSystem();
		}
	}

	void EnqueueNewSystem()
	{
		teleportParticles.Enqueue(((GameObject)GameObject.Instantiate(teleportParticleEffectPrefab.gameObject)).GetComponent<TeleportParticle>());
	}

	// Update is called once per frame
	void Update () 
	{
	
	}
}
