﻿using UnityEngine;
using System.Collections;

public class BlastParticle : MonoBehaviour {
	ParticleSystem mParticleSystem;
	bool isPlaying = false;
	// Use this for initialization
	void Awake () 
	{
		mParticleSystem = particleSystem;
		mParticleSystem.Stop();
	}

	public void BeginEffect(Vector3 position, Color color)
	{
		mParticleSystem.startColor = color;
		transform.position = position;

		mParticleSystem.Play();
		isPlaying = true;
	}
	
	void FixedUpdate()
	{
		if(isPlaying && !mParticleSystem.isPlaying)
		{
			mParticleSystem.Stop();
			BlastParticlePool.ReQueue(this);
			isPlaying = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
