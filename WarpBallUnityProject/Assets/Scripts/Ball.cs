﻿using UnityEngine;
using Fabric;
using System.Collections;

public class Ball : Photon.MonoBehaviour {
	const float minFalloffDistance = 1.2f;
	const float maxExplosionDistance = 1.6f;
	const float explosionStrength = 110.0f;
	public Transform particleTrailTransform;

	Renderer mRenderer;
	float reappearTime = 0;
	Vector2 spawnVelocity = Vector2.zero;

	WarpPlayer lastPlayerTouched = null;
	WarpBallobjectNetworkInterface mNet = null;
	// Use this for initialization
	void Awake () {
		lastPlayerTouched = null;
		mRenderer = GetComponentInChildren<Renderer>();
	}

	public void SetInterface(WarpBallobjectNetworkInterface interf)
	{
		mNet = interf;
	}

	void OnCollisionEnter2D(Collision2D collision) 
	{
		WarpPlayer otherPlayer = collision.gameObject.GetComponent<WarpPlayer>();

		if(otherPlayer != null)
		{
			lastPlayerTouched = otherPlayer;
			otherPlayer.IncreaseBlastCharge();
			/*if(mNet!= null && !mNet.isMine)
			{
				mNet.SendVelocityChange(transform.position,rigidbody2D.velocity);
			}*/
		}

		if(PhotonNetwork.isMasterClient)
		{
			photonView.RPC("AddNewState",PhotonTargets.Others,PhotonNetwork.time,transform.position,rigidbody2D.velocity);
		}

		if(collision.relativeVelocity.magnitude > .5f)
		{
			ShakeCamera.Shake((collision.relativeVelocity.magnitude/200.0f));
			EventManager.Instance.PostEvent("BallBounce");
		}


	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		Goal g = other.GetComponent<Goal>();
		if(g!=null)
		{
			MatchController.Score(g);
		}
	}

	[RPC]
	public void ResetPosition(Vector3 position, Vector2 newVelocity)
	{
		rigidbody2D.velocity = Vector2.zero;
		spawnVelocity = newVelocity;
		mRenderer.enabled = false;
		particleTrailTransform.particleSystem.enableEmission = false;
		TeleportParticlePool.CreateSlowTeleportEffect(transform.position,position,Color.white);
		transform.position = position;
		reappearTime  = Time.time + WarpPlayer.RESET_EFFECT_TIME;		
	}

	public void ApplyExplosion(WarpPlayer player)
	{
		Vector2 myPos = new Vector2(transform.position.x,transform.position.y);

		Vector2 playerPos = new Vector2(player.transform.position.x,player.transform.position.y);

		Vector2 forceDirection = (myPos-playerPos);
		float distance = forceDirection.magnitude;

		if(distance > maxExplosionDistance)
		{
			return;
		}
		float multiplier =  distance < minFalloffDistance ? 1 : (maxExplosionDistance-distance)/(maxExplosionDistance-minFalloffDistance) ;

		float forceStrength = explosionStrength * multiplier;
		rigidbody2D.AddForceAtPosition(forceStrength *forceDirection.normalized, playerPos);
		lastPlayerTouched = player;
	}

	void FixedUpdate()
	{
		if(mRenderer.enabled == false && reappearTime < Time.time)
		{
			rigidbody2D.velocity = spawnVelocity;
			mRenderer.enabled = true;
			particleTrailTransform.particleSystem.enableEmission = true;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		float rot = Mathf.Atan2(rigidbody2D.velocity.y,rigidbody2D.velocity.x);
		particleTrailTransform.localRotation = Quaternion.RotateTowards(particleTrailTransform.localRotation, Quaternion.Euler(0,0,rot * Mathf.Rad2Deg),400*Time.deltaTime);
	}
}
