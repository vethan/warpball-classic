﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class CreditsScreenController : MonoBehaviour {

	public GameObject mainMenuScreen;
	bool readyForAction = false;

	void OnEnable () 
	{
		GetComponent<UIPanel>().alpha = 0;
		TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {readyForAction = true;};
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!readyForAction)
		{
			return;
		}
		
		GamePadState testState = GamePad.GetState(WarpBall.activeController);
		
		if(testState.IsConnected == false)
		{
			for(int i = 0; i < 4; i++)
			{
				PlayerIndex testIndex = (PlayerIndex)i;
				testState = GamePad.GetState(testIndex);
				if(testState.IsConnected == true)
				{
					WarpBall.activeController = testIndex;
					return;
				}
			}
		}

		if(testState.Buttons.B == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("cancelSound");
			mainMenuScreen.SetActive(true);
			readyForAction = false;
			TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
		}

	}

}
