﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class PlayerEntryLine : MonoBehaviour {
	public UISprite indicatorIcon;
	public GameObject entryLine;
	public GameObject exitLine;
	public GameObject onlineLine;
	PlayerIndex mIndex;
	public bool isFree
	{
		get
		{
			return mIsFree;
		}
	}

	bool mIsFree = true;
	// Use this for initialization
	void Start () 
	{

	}

	public PlayerIndex GetIndex()
	{
		return mIndex;
	}

	public void PlayerLeft()
	{
		entryLine.SetActive(true);
		exitLine.SetActive(false);
		if(onlineLine!=null)
			onlineLine.SetActive(false);
		indicatorIcon.gameObject.SetActive(false);
		mIsFree = true;
	}

	public void OnlinePlayerEntered(string name)
	{
		entryLine.SetActive(false);
		exitLine.SetActive(false);
		onlineLine.SetActive(true);
		UILabel label = onlineLine.GetComponentInChildren<UILabel>();
		label.text = name;
		mIsFree = false;
		//ShowIndicatorIcon(activePlayer);
		//mIndex = activePlayer;
	}

	public void PlayerEntered(PlayerIndex activePlayer)
	{
		entryLine.SetActive(false);
		exitLine.SetActive(true);
		if(onlineLine!=null)
			onlineLine.SetActive(false);
		mIsFree = false;
		ShowIndicatorIcon(activePlayer);
		mIndex = activePlayer;
	}

	public void ShowIndicatorIcon(PlayerIndex activePlayer)
	{
		indicatorIcon.gameObject.SetActive(true);
		switch(activePlayer)
		{
		case PlayerIndex.One:
			indicatorIcon.spriteName = "xboxring1";
			break;
		case PlayerIndex.Two:
			indicatorIcon.spriteName = "xboxring2";
			break;
		case PlayerIndex.Three:
			indicatorIcon.spriteName = "xboxring3";
			break;
		case PlayerIndex.Four:
			indicatorIcon.spriteName = "xboxring4";
			break;
		}
	}


	// Update is called once per frame
	void Update () {
	
	}
}
