﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class GameOverPanelController : MonoBehaviour {
	bool readyForAction = false;
	public ParticleSystem mParticles;
	public UILabel winTeamLabel;
	public GameObject bottomAnchor;
	public GameObject rematchAnchor;
	public GameObject continueAnchor;
	static GameOverPanelController instance;

	public static void ShowGameOverScreen (int winningTeam)
	{
		if(instance == null)
		{
			print("No Instance");
			return;
		}
		instance.ShowSelf(winningTeam);
	}

	public void ShowSelf(int winningTeam)
	{
		winTeamLabel.text = "TEAM " + winningTeam + " Wins!";
		gameObject.SetActive(true);
		if(!PhotonNetwork.offlineMode)
		{
			rematchAnchor.SetActive(false);
			continueAnchor.transform.localPosition = new Vector3(0,continueAnchor.transform.localPosition.y,0);
		}
	}

	// Use this for initialization
	void Awake () {
		instance = this;
		gameObject.SetActive(false);
	}

	void OnEnable () 
	{
		GetComponent<UIPanel>().alpha = 0;
		TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {readyForAction = true;};
		mParticles.Play();
	}

	// Update is called once per frame
	void Update () {
		if(!readyForAction || !PhotonNetwork.isMasterClient)
		{
			return;
		}
		
		for(int i = 0; i < 4; i++)
		{
			PlayerIndex testPlayerIndex = (PlayerIndex)i;
			GamePadState testState = GamePad.GetState(testPlayerIndex);
			if (!testState.IsConnected)
			{
				continue;
			}
			
			if(testState.Buttons.A == ButtonState.Pressed)
			{
				if(PhotonNetwork.offlineMode == true)
				{
					PhotonNetwork.DestroyAll();
					PhotonNetwork.LeaveRoom();
					PhotonNetwork.offlineMode = false;
					TitleScreenMenu.startUp = TitleScreenMenu.StartupChoice.MatchMenu;
				}
				else
				{
					TitleScreenMenu.startUp = TitleScreenMenu.StartupChoice.OnlineMenu;
				}
				PhotonNetwork.LoadLevel("MainMenu");
			}		

			if(!PhotonNetwork.offlineMode)
			{
				return;
			}
				
			if(testState.Buttons.Y == ButtonState.Pressed)
			{
				PhotonNetwork.DestroyAll();
				PhotonNetwork.LeaveRoom();
				PhotonNetwork.offlineMode = false;
				MatchController.isRematch = true;
				PhotonNetwork.LoadLevel(Application.loadedLevel);
			}				
		}
		
	}
}
