﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;

public class GameListController : MonoBehaviour {
	const float ENTRY_Y_INTERVAL = 60;
	public UILabel bigLabel;
	public UIInput playerNameInput;
	public UIInput gameNameInput;
	public GameObject originalListEntry;
	public GameObject scrollContainer;

	public GameObject mainMenu;
	public GameObject onlineGameScreen;

	float startingY = 105;
	float targetY = 105;
	Vector3 mScrollRate = Vector3.zero;	
	float mTimeToTake = 0.3f;
	List<GameListEntry> availableGames = new List<GameListEntry>();
	int scrollIndex = 0;
	float scrollCounter = 0;
	bool readyForAction;

	void OnDisable()
	{
		PlayerPrefs.SetString(WarpBall.GAME_NAME_KEY,gameNameInput.text);
		PlayerPrefs.SetString(WarpBall.PLAYER_NAME_KEY,playerNameInput.text);
	}

	// Use this for initialization
	void OnEnable () 
	{
		gameNameInput.validator = InputValidator;
		if(PlayerPrefs.HasKey(WarpBall.GAME_NAME_KEY))
		{
			gameNameInput.text = PlayerPrefs.GetString(WarpBall.GAME_NAME_KEY);
		}
		playerNameInput.validator = InputValidator;
		if(PlayerPrefs.HasKey(WarpBall.PLAYER_NAME_KEY))
		{
			playerNameInput.text = PlayerPrefs.GetString(WarpBall.PLAYER_NAME_KEY);
		}

		GetComponent<UIPanel>().alpha = 0;
		TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {readyForAction = true;};
		originalListEntry.SetActive(false);
		if(PhotonNetwork.connectedAndReady)
		{
			RefreshGames();
		}
		else
		{
			bigLabel.text = "Connecting...";
			PhotonNetwork.ConnectUsingSettings("warpball");
		}

	}
	
	char InputValidator (string currentText, char nextChar)
	{
		if(!char.IsLetterOrDigit(nextChar) && nextChar != ' ')
		{
			return (char)0;
		}
		return nextChar;
	}

	void MoveUp()
	{
		if(scrollIndex == 0)
			return;
		Fabric.EventManager.Instance.PostEvent("MenuBeep");
		scrollIndex--;
		UpdateVisuals();
	}
	
	void MoveDown()
	{
		if(scrollIndex == availableGames.Count-1)
			return;
		Fabric.EventManager.Instance.PostEvent("MenuBeep");
		scrollIndex++;
		UpdateVisuals();
	}

	void UpdateVisuals()
	{
		if(availableGames.Count <= 0)
			return;

		availableGames[scrollIndex].selected = true;

		//Deactivate items either side
		if(scrollIndex > 0 && availableGames[scrollIndex-1].selected)
		{
			availableGames[scrollIndex-1].selected = false;
		}

		if(scrollIndex < availableGames.Count-1 && availableGames[scrollIndex+1].selected)
		{
			availableGames[scrollIndex+1].selected = false;
		}

		if(scrollIndex < 2)
		{
			targetY = startingY;
		}
		else
		{
			int gameToScrollTo = Mathf.Min(availableGames.Count-6,scrollIndex -2);
			targetY = startingY+ (gameToScrollTo * ENTRY_Y_INTERVAL);
		}
	}

	void OnJoinedLobby()
	{
		print("in a lobby");
		RefreshGames();
	}

	public void RefreshGames()
	{
		bigLabel.text = "Refreshing...";
		while(availableGames.Count > 0)
		{
			GameListEntry game = availableGames[0];
			Destroy(game.gameObject);
			availableGames.RemoveAt(0);
		}
		scrollIndex = 0;
		scrollContainer.transform.localPosition = new Vector3(0, startingY);
		RoomInfo[] list = PhotonNetwork.GetRoomList();
		if(list.Length == 0)
		{
			bigLabel.text = "No Games Found";
			return;
		}
		bigLabel.text = "";
		float yPos = 0;
		foreach(RoomInfo room in list)
		{
			GameObject g =  (GameObject)Instantiate(originalListEntry);
			g.SetActive(true);
			g.transform.parent = scrollContainer.transform;
			g.gameObject.layer = scrollContainer.layer;
			g.transform.localPosition = new Vector3(0,yPos,0);
			g.transform.localScale = Vector3.one;
			yPos -= ENTRY_Y_INTERVAL;
			GameListEntry entry = g.GetComponent<GameListEntry>();
			entry.SetUp(room);
			availableGames.Add(entry);
		}
		if(availableGames.Count > 0)
		{
			availableGames[scrollIndex].selected = true;
		}

	}

	void OnJoinedRoom()
	{
		onlineGameScreen.SetActive(true);
		TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
	}

	void OnReceivedRoomListUpdate()
	{
		if(availableGames.Count == 0)
			RefreshGames();
	}

	string GenerateUniqueGameName (string text, int i)
	{
		string nameGenned = text;
		if(i > 0)
		{
			nameGenned = nameGenned + i.ToString();
		}

		RoomInfo[] rooms = PhotonNetwork.GetRoomList();
		foreach(RoomInfo room in rooms)
		{
			if(room.name == nameGenned)
			{
				return GenerateUniqueGameName(text,i+1);
			}
		}
		return nameGenned;

	}



	// Update is called once per frame
	void Update () {
		if(!readyForAction)
		{
			return;
		}
		
		GamePadState testState = GamePad.GetState(WarpBall.activeController);
		
		if(testState.IsConnected == false)
		{
			for(int i = 0; i < 4; i++)
			{
				PlayerIndex testIndex = (PlayerIndex)i;
				testState = GamePad.GetState(testIndex);
				if(testState.IsConnected == true)
				{
					WarpBall.activeController = testIndex;
					return;
				}
			}
		}
		
		if(testState.Buttons.A == ButtonState.Pressed && PhotonNetwork.insideLobby)
		{
			PhotonNetwork.playerName = playerNameInput.text;
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			if(scrollIndex < availableGames.Count)
			{
				PhotonNetwork.JoinRoom(availableGames[scrollIndex].roomInfo.name);
			}
		}

		if(testState.Buttons.X == ButtonState.Pressed && PhotonNetwork.insideLobby)
		{
			PhotonNetwork.playerName = playerNameInput.text;

			string gameName = GenerateUniqueGameName(gameNameInput.text,0);
			//Generate default roomoptions
			RoomOptions newOptions = new RoomOptions();
			string[] s = new string[2];
			s[0] = "Fill";
			s[1] = "Type";
			newOptions.customRoomPropertiesForLobby = s;
			newOptions.customRoomProperties = new ExitGames.Client.Photon.Hashtable(2);
			newOptions.customRoomProperties["Fill"] = 1;
			newOptions.customRoomProperties["Type"] = "Score";
			newOptions.maxPlayers = 4;
			newOptions.isOpen =true;
			newOptions.isVisible = true;

			PhotonNetwork.CreateRoom(gameName,newOptions,TypedLobby.Default);

			bigLabel.text = "Creating room...";
			readyForAction = false;
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
		}

		if(testState.Buttons.Y == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			RefreshGames();
		}

		if(testState.Buttons.B == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("cancelSound");
			mainMenu.SetActive(true);
			readyForAction = false;
			PhotonNetwork.Disconnect();
			TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
		}
		
		if(Mathf.Abs(testState.ThumbSticks.Left.Y) < MainMenuController.minimumMovement)
		{
			scrollCounter = 0;
		}
		else
		{
			if(scrollCounter == 0)
			{
				if(testState.ThumbSticks.Left.Y > 0)
				{
					MoveUp();
				}
				else
				{
					MoveDown();
				}
			}
			else
			{
				if(Mathf.Abs(scrollCounter) > 1)
				{
					if(testState.ThumbSticks.Left.Y>0)
					{
						MoveUp();
					}
					else
					{
						MoveDown();
					}
					float sign = Mathf.Sign(scrollCounter);
					scrollCounter+=sign*(-1);
					if(scrollCounter == 0)
					{
						scrollCounter = 0.05f * sign;
					}

				}
			}
			scrollCounter += testState.ThumbSticks.Left.Y*Time.deltaTime*5;
		}

		if(scrollContainer.transform.localPosition.y != targetY)
		{
			scrollContainer.transform.localPosition = Vector3.SmoothDamp(scrollContainer.transform.localPosition,
			                                                             new Vector3(scrollContainer.transform.localPosition.x,targetY,scrollContainer.transform.localPosition.z),
			                                                             ref mScrollRate,
			                                                             mTimeToTake);
		}

	}
}
