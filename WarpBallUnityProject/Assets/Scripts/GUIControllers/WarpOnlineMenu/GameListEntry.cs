﻿using UnityEngine;
using System.Collections;

public class GameListEntry : MonoBehaviour {
	public UILabel gameName;
	public UILabel gameFill;
	public UILabel gameType;
	public UITweener selectedEffect;

	RoomInfo mRoomInfo;
	// Use this for initialization
	void Start () {
	
	}

	public RoomInfo roomInfo
	{
		get
		{
			return mRoomInfo;
		}
	}

	public bool selected
	{
		set
		{
			if(!value)
			{
				selectedEffect.Reset();
				transform.localScale = Vector3.one;
			}
			selectedEffect.enabled = value;
		}
		get
		{
			return selectedEffect.enabled;
		}
	}


	public void SetUp(RoomInfo info)
	{
		mRoomInfo = info;
		gameName.text = info.name.Length > 23 ? info.name.Substring(20) + "..." : info.name;
		gameFill.text = info.customProperties["Fill"] + "/4";
		gameType.text = info.customProperties["Type"].ToString();
	}

	// Update is called once per frame
	void Update () {
	
	}
}
