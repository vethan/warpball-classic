﻿using UnityEngine;
using System.Collections;

public class ScoreMonitor : MonoBehaviour {
	public UILabel[] scoreLabels;

	public UILabel timeLabel;
	int teamCount = 0;
	// Use this for initialization
	void Start () {
		teamCount = scoreLabels.Length;
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i < teamCount; i++)
		{
			scoreLabels[i].text = MatchController.GetTeamScore(i).ToString();
		}
		timeLabel.text = MatchController.GetTime();
	}
}
