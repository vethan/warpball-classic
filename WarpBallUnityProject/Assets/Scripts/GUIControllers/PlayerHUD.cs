﻿using UnityEngine;
using System.Collections;

public class PlayerHUD : MonoBehaviour {

	public UISprite mSprite;
	public UILabel mLabel;

	float currentChargeValue;
	int targetChargeValue;


	float changeVelocity;
	public void SetColor(Color newColor)
	{
		mSprite.color = newColor;
	}

	public void SetCharge(int newCharge)
	{
		targetChargeValue = newCharge;
	}

	// Use this for initialization
	void Start () 
	{
		mLabel.gameObject.SetActive(false);
		//mLabel.text = ((int)currentChargeValue) + "%";
	}
	
	// Update is called once per frame
	void UpdateOld () 
	{
		if(currentChargeValue != targetChargeValue)
		{
			currentChargeValue = Mathf.Lerp(currentChargeValue,targetChargeValue,Time.deltaTime*4);
			if(Mathf.Abs(currentChargeValue-targetChargeValue)<0.5f)
			{
				currentChargeValue = targetChargeValue;
			}
			mLabel.text = ((int)currentChargeValue) + "%";
		}
	}
}
