﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class TitleScreenMenu : MonoBehaviour 
{
	public enum StartupChoice {None, OnlineMenu, MatchMenu};
	public static StartupChoice startUp = StartupChoice.None;

	bool readyForAction = false;
	public GameObject MainMenu;
	public GameObject onlineMatchMenu;
	public GameObject matchMenu;

	void OnEnable () 
	{
		GetComponent<UIPanel>().alpha = 0;
		switch(startUp)
		{
		case StartupChoice.None:
			TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {readyForAction = true;};
			return;
		case StartupChoice.MatchMenu:
			matchMenu.SetActive(true);
			break;

		case StartupChoice.OnlineMenu:
			onlineMatchMenu.SetActive(true);
			break;
		}

		startUp = StartupChoice.None;
		gameObject.SetActive(false);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!readyForAction)
		{
			return;
		}

		for(int i = 0; i < 4; i++)
		{
			PlayerIndex testPlayerIndex = (PlayerIndex)i;
			GamePadState testState = GamePad.GetState(testPlayerIndex);
			if (!testState.IsConnected)
			{
				continue;
			}

			if(testState.Buttons.Start == ButtonState.Pressed)
			{
				Fabric.EventManager.Instance.PostEvent("MenuBeep");
				MainMenu.SetActive(true);
				readyForAction = false;
				WarpBall.activeController = testPlayerIndex;
				TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
			}				
		}

	}
}
