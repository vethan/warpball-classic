﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;


public class MatchSettingsMenu : MonoBehaviour {

	public List<PlayerEntryLine> playerEntries = new List<PlayerEntryLine>();

	public UILabel gameTypeLabel;
	public UILabel gameLimitLabel ;
	public UILabel teamsLabel;

	public GameObject pressStartLine;

	public GameObject mainMenu;

	protected Dictionary<PlayerIndex,GamePadState> lastGamepadState = new Dictionary<PlayerIndex, GamePadState>();
	protected Dictionary<PlayerIndex, int> playerEntryMap = new Dictionary<PlayerIndex, int>();
	string gameTypePrefix = "Game Type: ";
	string timeLimitPrefix = "Time Limit: ";
	string scoreLimitPrefix = "Score Limit: ";
	public enum GameType {TimedGame, ScoreGame, Max};
	public enum ScoreLimit {Five,Ten,Fifteen,Twenty, Max};
	public enum TimeLimit {Two,Five,Eight,Ten,Twelve, Max};
	public enum TeamDivider {One,Two,Three,Max};

	protected int intLimit = 10;

	protected ScoreLimit mScoreLimit = ScoreLimit.Ten;
	protected TimeLimit mTimeLimit = TimeLimit.Two;
	protected TeamDivider mTeamDivider = TeamDivider.One;
	protected GameType mGameType = GameType.ScoreGame;
	protected bool readyForAction = false;
	protected List<int>[] mTeams;
	// Use this for initialization
	void OnEnable () 
	{
		GetComponent<UIPanel>().alpha = 0;
		TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {readyForAction = true;};
		playerEntryMap[WarpBall.activeController] = 0;
		playerEntries[0].PlayerEntered(WarpBall.activeController);
		for(int i = 1; i < playerEntries.Count; i++)
		{
			playerEntries[i].PlayerLeft();
		}
		pressStartLine.SetActive(false);
		UpdateVisuals();
	}

	protected void QuitToMenu()
	{
		readyForAction = false;
		mainMenu.SetActive(true);
		TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
	}


	void UpdateTimeTypeVisual()
	{
		string labelStart = timeLimitPrefix;
		
		switch(mTimeLimit)
		{
		case TimeLimit.Two:
			labelStart += "2mins";
			intLimit = 2;
			break;
		case TimeLimit.Five:
			labelStart += "5mins";
			intLimit = 5;
			break;
		case TimeLimit.Eight:
			labelStart += "8mins";
			intLimit = 8;
			break;
		case TimeLimit.Ten:
			labelStart += "10mins";
			intLimit = 10;
			break;
		case TimeLimit.Twelve:
			labelStart += "12mins";
			intLimit = 12;
			break;
		}
		gameLimitLabel.text = labelStart;
	}

	void UpdateScoreTypeVisual()
	{
		string labelStart = scoreLimitPrefix;

		switch(mScoreLimit)
		{
		case ScoreLimit.Five:
			labelStart += "5";
			intLimit = 5;
			break;
		case ScoreLimit.Ten:
			labelStart += "10";
			intLimit = 10;
			break;
		case ScoreLimit.Fifteen:
			labelStart += "15";
			intLimit = 15;
			break;
		case ScoreLimit.Twenty:
			labelStart += "20";
			intLimit = 20;
			break;
		}
		gameLimitLabel.text = labelStart;
	}

	void UpdateTeamLabel()
	{
		if(mGameType == GameType.Max)
		{
			//TODO: all - v - all code
			teamsLabel.text = "";
			return;
		}

		int divider = (int)mTeamDivider;

		//Initialize teams stuff
		List<int>[] teams = new List<int>[2];
		for(int i = 0; i < 2; i++)
		{
			teams[i] = new List<int>();
		}
		List<int> playersAvailable = new List<int>();
		for(int i = 0; i < playerEntries.Count; i++)
		{
			if(!playerEntries[i].isFree)
				playersAvailable.Add(i);
		}

		if(playersAvailable.Count < 2)
		{
			teamsLabel.text = "";
			return;
		}

		switch(playersAvailable.Count)
		{
		case 2:
			teams[0].Add(playersAvailable[0]);
			break;
		case 3:
			teams[0].Add(playersAvailable[divider]);
			break;
		case 4:
			teams[0].Add(playersAvailable[0]);
			teams[0].Add(playersAvailable[divider+1]);
			break;
		}
		for(int i = 0; i < playersAvailable.Count; i++)
		{
			if(!teams[0].Contains(playersAvailable[i]))
			{
				teams[1].Add(playersAvailable[i]);
			}
		}

		string teamText = "";
		foreach(int i in teams[0])
		{
			teamText += "P" + (i+1) + " & ";
		}
		teamText = teamText.Substring(0,teamText.Length-2) + "Vs ";
		foreach(int i in teams[1])
		{
			teamText += "P" + (i+1) + " & ";
		}

		teamText = teamText.Substring(0,teamText.Length-2);
		teamsLabel.text = teamText;

		mTeams = teams;
	}
	
	protected void UpdateVisuals()
	{
		switch(mGameType)
		{
		case GameType.ScoreGame:
			gameTypeLabel.text = gameTypePrefix + "Score";
			UpdateScoreTypeVisual();
			break;
		case GameType.TimedGame:
			gameTypeLabel.text = gameTypePrefix + "Time";
			UpdateTimeTypeVisual();
			break;
		}
		UpdateTeamLabel();
	}

	void HandleConnectedInput(PlayerIndex index, GamePadState padState)
	{
		if(padState.Buttons.B == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("cancelSound");
			playerEntries[playerEntryMap[index]].PlayerLeft();
			playerEntryMap.Remove(index);
			UpdateVisuals();
			if(playerEntryMap.Count < 2)
			{
				pressStartLine.SetActive(false);
			}
			return;
		}

		if(padState.Buttons.Start == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			MatchControlPacket matchPacket = new GameObject("MatchPacket").AddComponent<MatchControlPacket>();
			matchPacket.SetUpCoreData(mGameType,intLimit);
			for(int i =0; i < 4; i++)
			{
				if(!playerEntries[i].isFree)
				{
					matchPacket.SetPlayer(i,playerEntries[i].GetIndex(),mTeams[0].Contains(i) ? 0 : 1);
				}
			}
			MatchController.isRematch = false;
			Application.LoadLevel("GameScreen");
		}

		bool wasDirtied = false;

		if(padState.Buttons.Y == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.Y == ButtonState.Released))
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			mGameType = (GameType)(((int)mGameType+1)%(int)GameType.Max);
			wasDirtied = true;
		}

		if(padState.Buttons.X == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.X == ButtonState.Released))
		{
			//TODO: Open Variants Menu
			//mGameType = (GameType)(((int)mGameType+1)%(int)GameType.Max);
			//wasDirtied = true;
		}

		if(padState.Buttons.RightShoulder == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.RightShoulder == ButtonState.Released))
		{
			mTeamDivider = (TeamDivider)(((int)mTeamDivider+1)%(int)TeamDivider.Max);
			wasDirtied = true;
		}
		if(padState.Buttons.LeftShoulder == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.LeftShoulder == ButtonState.Released))
		{
			if((int)mTeamDivider == 0)
				mTeamDivider = TeamDivider.Max;
			mTeamDivider = (TeamDivider)(((int)mTeamDivider-1)%(int)TeamDivider.Max);
			wasDirtied = true;
		}
		
		if(padState.DPad.Right == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].DPad.Right == ButtonState.Released))
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			if(mGameType == GameType.ScoreGame)
			{
				mScoreLimit = (ScoreLimit)(((int)mScoreLimit+1)%(int)ScoreLimit.Max);
			}
			else if(mGameType == GameType.TimedGame)
			{
				mTimeLimit = (TimeLimit)(((int)mTimeLimit+1)%(int)TimeLimit.Max);
			}
			wasDirtied = true;
		}
				
		if(padState.DPad.Left == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].DPad.Left == ButtonState.Released))
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			if(mGameType == GameType.ScoreGame)
			{
				if((int)mScoreLimit == 0)
					mScoreLimit = ScoreLimit.Max;
				mScoreLimit = (ScoreLimit)(((int)mScoreLimit-1)%(int)ScoreLimit.Max);
			}
			else if(mGameType == GameType.TimedGame)
			{
				if((int)mTimeLimit == 0)
					mTimeLimit = TimeLimit.Max;
				mTimeLimit = (TimeLimit)(((int)mTimeLimit-1)%(int)TimeLimit.Max);
			}
			wasDirtied = true;
		}

		if(wasDirtied)
		{
			UpdateVisuals();
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if(!readyForAction)
		{
			return;
		}

		for(int i = 0; i < 4; i++)
		{
			PlayerIndex testPlayerIndex = (PlayerIndex)i;
			GamePadState testState = GamePad.GetState(testPlayerIndex);
			if (!testState.IsConnected)
			{
				continue;
			}

			if(playerEntryMap.ContainsKey(testPlayerIndex))
			{
				HandleConnectedInput(testPlayerIndex,testState);
			}
			else
			{
				if(testState.Buttons.A == ButtonState.Pressed)
				{
					Fabric.EventManager.Instance.PostEvent("MenuBeep");
					foreach(PlayerEntryLine l in playerEntries)
					{
						if(l.isFree)
						{
							l.PlayerEntered(testPlayerIndex);
							playerEntryMap[testPlayerIndex] = playerEntries.IndexOf(l);
							UpdateVisuals();
							if(playerEntryMap.Count >= 2)
							{
								pressStartLine.SetActive(true);
							}
							break;
						}
					}
				}
				if(testPlayerIndex == WarpBall.activeController && testState.Buttons.B == ButtonState.Pressed && (!lastGamepadState.ContainsKey(testPlayerIndex) || lastGamepadState[testPlayerIndex].Buttons.B == ButtonState.Released))
				{
					Fabric.EventManager.Instance.PostEvent("cancelSound");
					QuitToMenu();
				}
			}
			lastGamepadState[testPlayerIndex] = testState;
		}
	}
}
