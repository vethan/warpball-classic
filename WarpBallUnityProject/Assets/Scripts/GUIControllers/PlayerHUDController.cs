﻿using UnityEngine;
using System.Collections;

public class PlayerHUDController : MonoBehaviour {
	public PlayerHUD[] playerHUDs;
	static PlayerHUDController instance;

	public static void SetPlayerColor(int index, Color c)
	{
		if(instance == null)
			return;

		instance.playerHUDs[index].gameObject.SetActive(true);
		instance.playerHUDs[index].SetColor(c);
	}

	public static void SetPlayerCharge(int index, int chargeAmount)
	{
		if(instance == null)
			return;
		
		instance.playerHUDs[index].SetCharge(chargeAmount);
	}

	public static void DisableHUD(int index)
	{
		if (instance == null)
			return;

		instance.playerHUDs[index].gameObject.SetActive(false);
	}

	// Use this for initialization
	void Awake () 
	{
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
