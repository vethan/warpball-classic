﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class InstructionsPanelController : MonoBehaviour {
	bool readyForAction = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(readyForAction)
		{
			for(int i = 0; i < 4; i++)
			{
				PlayerIndex testPlayerIndex = (PlayerIndex)i;
				GamePadState testState = GamePad.GetState(testPlayerIndex);
				if (!testState.IsConnected)
				{
					continue;
				}
				
				if(testState.Buttons.Start == ButtonState.Pressed)
				{
					readyForAction = false;
					WarpBall.activeController = testPlayerIndex;
					TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {MatchController.StartGame(); gameObject.SetActive(false);};
				}				
			}
		}
	}
}
