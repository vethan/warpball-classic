﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;


public class MainMenuController : MonoBehaviour 
{
	public const float minimumMovement = 0.2F;
	bool readyForAction = false;

	float scrollCounter = 0;
	int lastScrollInt = 0;
	public enum MainMenuOption {Start,Online, Practice,Exit, Credits, Max};

	int menuOption = 0;

	public UITweener startOption;
	public UITweener onlineOption;
	public UITweener practiceOption;
	public UITweener creditsOption;
	public UITweener exitOption;

	public GameObject matchSettingMenu;
	public GameObject titleScreen;
	public GameObject onlineScreen;
	public GameObject creditsScreen;

	void OnEnable () 
	{
		menuOption = 0;
		GetComponent<UIPanel>().alpha = 0;
		TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {readyForAction = true;};
		UpdateVisuals();
	}

	// Use this for initialization
	void Start () 
	{
	
	}

	void UpdateVisuals()
	{
		switch((MainMenuOption)menuOption)
		{
		case MainMenuOption.Start:
			startOption.enabled = true;
			onlineOption.Reset();
			onlineOption.transform.localScale = Vector3.one;
			onlineOption.enabled = false;
			creditsOption.Reset();
			creditsOption.transform.localScale = Vector3.one;
			creditsOption.enabled = false;
			break;
		case MainMenuOption.Online:
			onlineOption.enabled = true;
			startOption.Reset();
			startOption.transform.localScale = Vector3.one;
			startOption.enabled = false;
			practiceOption.Reset();
			practiceOption.transform.localScale = Vector3.one;
			practiceOption.enabled = false;
			break;
		case MainMenuOption.Practice:
			practiceOption.enabled = true;
			onlineOption.Reset();
			onlineOption.transform.localScale = Vector3.one;
			onlineOption.enabled = false;
			exitOption.Reset();
			exitOption.transform.localScale = Vector3.one;
			exitOption.enabled = false;
			break;
		case MainMenuOption.Exit:
			exitOption.enabled = true;
			practiceOption.Reset();
			practiceOption.transform.localScale = Vector3.one;
			practiceOption.enabled = false;
			creditsOption.Reset();
			creditsOption.transform.localScale = Vector3.one;
			creditsOption.enabled = false;
			break;
		case MainMenuOption.Credits:
			creditsOption.enabled = true;
			startOption.Reset();
			startOption.transform.localScale = Vector3.one;
			startOption.enabled = false;
			exitOption.Reset();
			exitOption.transform.localScale = Vector3.one;
			exitOption.enabled = false;
			break;

		}
	}

	void MoveUp()
	{
		Fabric.EventManager.Instance.PostEvent("MenuBeep");
		menuOption--;
		if(menuOption < 0)
		{
			menuOption += (int)MainMenuOption.Max;
		}
		UpdateVisuals();
	}

	void MoveDown()
	{
		Fabric.EventManager.Instance.PostEvent("MenuBeep");
		menuOption++;
		menuOption = menuOption%(int)MainMenuOption.Max;
		UpdateVisuals();
	}

	// Update is called once per frame
	void Update () 
	{
		if(!readyForAction)
		{
			return;
		}

		GamePadState testState = GamePad.GetState(WarpBall.activeController);

		if(testState.IsConnected == false)
		{
			for(int i = 0; i < 4; i++)
			{
				PlayerIndex testIndex = (PlayerIndex)i;
				testState = GamePad.GetState(testIndex);
				if(testState.IsConnected == true)
				{
					WarpBall.activeController = testIndex;
					return;
				}
			}
		}

		if(testState.Buttons.A == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			switch((MainMenuOption)menuOption)
			{
			case MainMenuOption.Start:
				matchSettingMenu.SetActive(true);
				readyForAction = false;
				TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
				break;
			case MainMenuOption.Online:
				onlineScreen.SetActive(true);
				readyForAction = false;
				TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
				break;
			case MainMenuOption.Practice:
				Application.LoadLevel("AIGameScreen");
				break;
			case MainMenuOption.Exit:
				Application.Quit();
				break;
			case MainMenuOption.Credits:
				creditsScreen.SetActive(true);
				readyForAction = false;
				TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
				break;
			}
		}

		if(testState.Buttons.B == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("cancelSound");
			titleScreen.SetActive(true);
			readyForAction = false;
			TweenAlpha.Begin(gameObject,WarpBall.FADE_OUT_TIME,0).onFinished += (UITweener tween) => {gameObject.SetActive(false);};
		}

		if(Mathf.Abs(testState.ThumbSticks.Left.Y) < minimumMovement)
		{
			scrollCounter = 0;
		}
		else
		{
			if(scrollCounter == 0)
			{
				if(testState.ThumbSticks.Left.Y > 0)
				{
					MoveUp();
				}
				else
				{
					MoveDown();
				}
			}
			else
			{
				if(Mathf.Abs(lastScrollInt) < Mathf.Abs((int)(scrollCounter+testState.ThumbSticks.Left.Y*Time.deltaTime*2)))
				{
					if(testState.ThumbSticks.Left.Y>0)
					{
						MoveUp();
					}
					else
					{
						MoveDown();
					}
				}
			}
			scrollCounter += testState.ThumbSticks.Left.Y*Time.deltaTime*2;
			lastScrollInt = (int)scrollCounter;
		}
	}
}
