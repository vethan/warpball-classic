﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;


public class OnlineMatchSettingsMenu : MatchSettingsMenu {

	protected int[] onlineEntryMap = new int[4];
	protected string[] onlineNameMap = new string[4];
	PhotonView mView;
	Dictionary<int,int> playerCounter = new Dictionary<int, int>();
	// Use this for initialization
	void OnEnable () 
	{
		mView = PhotonView.Get(this);
		GetComponent<UIPanel>().alpha = 0;
		TweenAlpha.Begin(gameObject,WarpBall.FADE_IN_TIME,1).onFinished += (UITweener tween) => {
			readyForAction = true;
			//Initialise after the twee so that we're pretty certain we've received an update from the server
			if(!PhotonNetwork.isMasterClient)
			{
				bool gotASlot = false;
				//Iterate through all slots, to find a free one, and update visuals
				for(int i = 0; i < 4; i++)
				{
					if(onlineEntryMap[i] != -1)
					{
						playerEntries[i].OnlinePlayerEntered(onlineNameMap[i]);
					}
					else if(!gotASlot)
					{
						playerEntries[i].PlayerEntered(WarpBall.activeController);
						playerEntryMap[WarpBall.activeController] = 0;
						mView.RPC("RemoteControllerJoined",PhotonTargets.MasterClient,PhotonNetwork.player.ID,i,PhotonNetwork.playerName);
						gotASlot = true;
					}
					else
					{
						playerEntries[i].PlayerLeft();
					}
				}
				
				if(!gotASlot)
				{
					PhotonNetwork.LeaveRoom();
					QuitToMenu();
					return;
				}
				UpdateVisuals();
			}
		};

		if(PhotonNetwork.isMasterClient)
		{
			playerCounter.Clear();
			//Reset the online entry map
			for(int i = 1; i < 4; i++)
			{
				onlineEntryMap[i] = -1;
				onlineNameMap[i] = null;
			}
			//Reset give self a slot
			playerEntryMap[WarpBall.activeController] = 0;
			onlineEntryMap[0] = PhotonNetwork.player.ID;
			onlineNameMap[0] = PhotonNetwork.playerName;
			playerEntries[0].PlayerEntered(WarpBall.activeController);
			playerCounter[PhotonNetwork.player.ID] = 1;

			//Clear out remaining slots
			for(int i = 1; i < playerEntries.Count; i++)
			{
				playerEntries[i].PlayerLeft();
			}			
		}
		pressStartLine.SetActive(false);
		UpdateVisuals();
		UpdateRoomData();
	}

	bool AddNewController(PlayerIndex index)
	{
		for(int i = 0; i < 4; i++)
		{
			if(onlineEntryMap[i] == -1)
			{
				playerEntries[i].PlayerEntered(index);
				playerEntryMap[index] = i;
				if(PhotonNetwork.isMasterClient)
				{
					int amount = playerCounter.ContainsKey(PhotonNetwork.player.ID) ? playerCounter[PhotonNetwork.player.ID] : 0;
					string nameToDisplay = amount == 0 ? PhotonNetwork.playerName : PhotonNetwork.playerName + "(" + (amount+1) + ")";

					onlineEntryMap[i] = PhotonNetwork.player.ID;
					onlineNameMap[i] = nameToDisplay;
					playerCounter[PhotonNetwork.player.ID] = amount+1;
				}
				else
				{
					mView.RPC("RemoteControllerJoined",PhotonTargets.MasterClient,PhotonNetwork.player.ID,i,PhotonNetwork.playerName);
				}
				UpdateVisuals();
				return true;
			}
		}
		return false;
	}

	void ControllerLeft(PlayerIndex index)
	{
		int location = playerEntryMap[index];
		playerEntries[location].PlayerLeft();
		playerEntryMap.Remove(index);

		if(index == WarpBall.activeController)
		{
			PhotonNetwork.LeaveRoom();
			QuitToMenu();
			return;
		}

		if(PhotonNetwork.isMasterClient)
		{
			playerCounter[PhotonNetwork.player.ID]--;
			onlineEntryMap[location] = -1;
		}
		else
		{
			mView.RPC("RemoteControllerLeft",PhotonTargets.MasterClient,PhotonNetwork.player.ID,location);
		}
		UpdateVisuals();
		UpdateRoomData();
	}
	
	void HandleConnectedInput(PlayerIndex index, GamePadState padState)
	{
		if(padState.Buttons.B == ButtonState.Pressed)
		{
			Fabric.EventManager.Instance.PostEvent("cancelSound");
			ControllerLeft(index);
			CheckIfReady();
			return;
		}

		if(!PhotonNetwork.isMasterClient)
			return;

		if(padState.Buttons.Start == ButtonState.Pressed && index == WarpBall.activeController)
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			if(PhotonNetwork.isMasterClient)
			{
				PhotonNetwork.room.visible = false;
				PhotonNetwork.room.open = false;
			}
			mView.RPC("StartMatchNow",PhotonTargets.All);
			PhotonNetwork.LoadLevel("GameScreen");
		}

		bool wasDirtied = false;

		if(padState.Buttons.Y == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.Y == ButtonState.Released))
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			mGameType = (GameType)(((int)mGameType+1)%(int)GameType.Max);
			wasDirtied = true;
		}

		if(padState.Buttons.X == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.X == ButtonState.Released))
		{
			//TODO: Open Variants Menu
			//mGameType = (GameType)(((int)mGameType+1)%(int)GameType.Max);
			//wasDirtied = true;
		}

		if(padState.Buttons.RightShoulder == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.RightShoulder == ButtonState.Released))
		{
			mTeamDivider = (TeamDivider)(((int)mTeamDivider+1)%(int)TeamDivider.Max);
			wasDirtied = true;
		}
		if(padState.Buttons.LeftShoulder == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].Buttons.LeftShoulder == ButtonState.Released))
		{
			if((int)mTeamDivider == 0)
				mTeamDivider = TeamDivider.Max;
			mTeamDivider = (TeamDivider)(((int)mTeamDivider-1)%(int)TeamDivider.Max);
			wasDirtied = true;
		}
		
		if(padState.DPad.Right == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].DPad.Right == ButtonState.Released))
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			if(mGameType == GameType.ScoreGame)
			{
				mScoreLimit = (ScoreLimit)(((int)mScoreLimit+1)%(int)ScoreLimit.Max);
			}
			else if(mGameType == GameType.TimedGame)
			{
				mTimeLimit = (TimeLimit)(((int)mTimeLimit+1)%(int)TimeLimit.Max);
			}
			wasDirtied = true;
		}

		if(padState.DPad.Left == ButtonState.Pressed && (!lastGamepadState.ContainsKey(index) || lastGamepadState[index].DPad.Left == ButtonState.Released))
		{
			Fabric.EventManager.Instance.PostEvent("MenuBeep");
			if(mGameType == GameType.ScoreGame)
			{
				if((int)mScoreLimit == 0)
					mScoreLimit = ScoreLimit.Max;
				mScoreLimit = (ScoreLimit)(((int)mScoreLimit-1)%(int)ScoreLimit.Max);
			}
			else if(mGameType == GameType.TimedGame)
			{
				if((int)mTimeLimit == 0)
					mTimeLimit = TimeLimit.Max;
				mTimeLimit = (TimeLimit)(((int)mTimeLimit-1)%(int)TimeLimit.Max);
			}
			wasDirtied = true;
		}

		if(wasDirtied)
		{
			UpdateVisuals();
			UpdateRoomData();
		}
	}

	void UpdateRoomData()
	{
		if(!PhotonNetwork.isMasterClient)
		{
			ExitGames.Client.Photon.Hashtable r = PhotonNetwork.room.customProperties;
			int count = 0;
			for(int i = 0; i < 4; i++)
			{
				if(onlineEntryMap[i] != -1)
				{
					count++;
				}
			}
			r["Fill"] = count;
			r["Type"] = mGameType.ToString();
			PhotonNetwork.room.SetCustomProperties(r);
		}
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
	{
		if(PhotonNetwork.player.isMasterClient)
		{
			for(int i = 0; i < 4; i++)
			{
				if(onlineEntryMap[i] == otherPlayer.ID)
				{
					onlineEntryMap[i] = -1;
				}
			}
		}
	}

	void OnMasterClientSwitched (PhotonPlayer newMasterClient)
	{ 
		//This means the "host" left so leave the room
		PhotonNetwork.LeaveRoom();
		QuitToMenu();
	}

	void CheckIfReady()
	{
		if(!PhotonNetwork.isMasterClient)
		{
			return;
		}


		int count = 0;
		for(int i = 0; i < 4; i++)
		{
			if(onlineEntryMap[i] != -1)
			{
				count++;
			}
		}
		if(count >= 2)
		{
			pressStartLine.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!readyForAction)
		{
			return;
		}

		for(int i = 0; i < 4; i++)
		{
			PlayerIndex testPlayerIndex = (PlayerIndex)i;
			GamePadState testState = GamePad.GetState(testPlayerIndex);
			if (!testState.IsConnected)
			{
				if(playerEntryMap.ContainsKey(testPlayerIndex) && onlineEntryMap[i] == PhotonNetwork.player.ID)
				{
					ControllerLeft(testPlayerIndex);
				}
				continue;
			}

			if(playerEntryMap.ContainsKey(testPlayerIndex) && onlineEntryMap[i] == PhotonNetwork.player.ID)
			{
				HandleConnectedInput(testPlayerIndex,testState);
			}
			else
			{
				if(testState.Buttons.A == ButtonState.Pressed  && (!lastGamepadState.ContainsKey(testPlayerIndex) || lastGamepadState[testPlayerIndex].Buttons.A == ButtonState.Released) )
				{
					bool success = AddNewController(testPlayerIndex);
					if(success)
					{
						Fabric.EventManager.Instance.PostEvent("MenuBeep");
					}
					else
					{
						Fabric.EventManager.Instance.PostEvent("cancelSound");
					}
					CheckIfReady();
				}
				
				if(testPlayerIndex == WarpBall.activeController && testState.Buttons.B == ButtonState.Pressed && (!lastGamepadState.ContainsKey(testPlayerIndex) || lastGamepadState[testPlayerIndex].Buttons.B == ButtonState.Released))
				{
					Fabric.EventManager.Instance.PostEvent("cancelSound");
					PhotonNetwork.LeaveRoom();
					QuitToMenu();
				}
			}
			lastGamepadState[testPlayerIndex] = testState;
		}
	}

	[RPC]
	void StartMatchNow()
	{
		PhotonNetwork.automaticallySyncScene = true;
		MatchControlPacket matchPacket = new GameObject("MatchPacket").AddComponent<MatchControlPacket>();
		matchPacket.SetUpCoreData(mGameType,intLimit);
		for(int i =0; i < 4; i++)
		{
			if(onlineEntryMap[i] != -1)
			{
				if(onlineEntryMap[i] == PhotonNetwork.player.ID)
				{
					matchPacket.SetPlayer(i,playerEntries[i].GetIndex(),mTeams[0].Contains(i) ? 0 : 1);
				}
				else
				{
					matchPacket.SetPlayer(i,"Player",mTeams[0].Contains(i) ? 0 : 1);
				}
			}
		}
		MatchController.isRematch = true;
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(mGameType);
			stream.SendNext(mScoreLimit);
			stream.SendNext(mTimeLimit);
			stream.SendNext(mTeamDivider);
			for(int i = 0; i < 4; i++)
			{
				stream.SendNext(onlineNameMap[i] == null ? "" : onlineNameMap[i]);
				stream.SendNext(onlineEntryMap[i]);
			}
		} 
		else
		{
			mGameType = (GameType)stream.ReceiveNext();
			mScoreLimit = (ScoreLimit)stream.ReceiveNext();
			mTimeLimit = (TimeLimit)stream.ReceiveNext();
			mTeamDivider = (TeamDivider)stream.ReceiveNext();


			for(int i = 0; i < 4; i++)
			{
				onlineNameMap[i] = (string)stream.ReceiveNext();
				onlineEntryMap[i] = (int)stream.ReceiveNext();
				if(playerEntries[i].isFree && onlineEntryMap[i] != -1)
				{
					if(onlineEntryMap[i] == PhotonNetwork.player.ID)
					{
						Debug.LogError("Shouldn't be free if I'm in charge");
					}
					else
					{
							playerEntries[i].OnlinePlayerEntered(onlineNameMap[i]);
					}
				}
				else if(!playerEntries[i].isFree && onlineEntryMap[i] == -1)
				{
					if(onlineEntryMap[i] != PhotonNetwork.player.ID)
					{
						playerEntries[i].PlayerLeft();
					}
					else
					{
						Debug.LogWarning("Selected but map is empty");
					}
				}
			}
			UpdateVisuals();
		}
	}
	
	[RPC]
	void RemoteControllerLeft(int ID, int slot)
	{
		if(onlineEntryMap[slot] == ID)
		{
			onlineEntryMap[slot] = -1;
			playerEntries[slot].PlayerLeft();
			playerCounter[ID]--;
			UpdateVisuals();
		}
	}

	[RPC]
	void RemoteControllerJoined(int ID, int slot, string name)
	{
		if(onlineEntryMap[slot] == -1 || onlineEntryMap[slot] == ID)
		{
			bool increase = onlineEntryMap[slot] != ID;
			int amount = playerCounter.ContainsKey(ID) ? playerCounter[ID] : 0;
			string nameToDisplay = amount == 0 ? name : name + "(" + (amount+1) + ")";
			onlineEntryMap[slot] = ID;
			onlineNameMap[slot] = nameToDisplay;
			playerEntries[slot].OnlinePlayerEntered(nameToDisplay);
			playerCounter[ID] = increase ? (amount+1) : amount;
			UpdateVisuals();
		}
	}
}
