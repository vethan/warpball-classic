﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour {

	static ShakeCamera instance = null;
	float decreaseFactor = 1.0f;
	Vector3 defaultPlace;
	public static void Shake(float startPoint)
	{
		if(instance == null)
			return;
		instance.StartCoroutine(instance.StartShake(startPoint));
	}
	
	IEnumerator StartShake(float startPoint)
	{

		float shake = startPoint;
		while(shake > 0)
		{
			Vector2 shakeplace = Random.insideUnitCircle * shake; 
			transform.position = defaultPlace + new Vector3(shakeplace.x,shakeplace.y,0);
			shake = Mathf.Max(0,shake-(Time.deltaTime * decreaseFactor));
			yield return null;
		}
		transform.position = defaultPlace;
		yield break;
	}

	// Use this for initialization
	void Start () 
	{
		defaultPlace = transform.position;
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
