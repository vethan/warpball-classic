﻿Shader "Custom/Circle-Border" {
	Properties {
		[PerRendererData]_MainColor ("Color", Color) = (1,1,1,1)
		_CircleCenterX ("Circle Center X", Range(0,1)) = 0.5
		_CircleCenterY ("Circle Center Y", Range(0,1)) = 0.5
		_CircleRadius ("Radius Size",Range(0,1)) = 0.5
	}
	SubShader 
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		Blend SrcAlpha One
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			
			sampler2D _MainTex;
			float _CircleCenterX;
			float _CircleCenterY;
			float _CircleRadius;
			float4 _MainColor;
			
			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};

			fragmentInput vert(appdata_base i)
			{
				fragmentInput o;
				o.pos = mul(UNITY_MATRIX_MVP,i.vertex);
				o.uv = i.texcoord.xy;
				return o;
			}

			half4 frag(fragmentInput i) : COLOR
			{
				float2 uv = float2(i.uv.x -  _CircleCenterX, i.uv.y - _CircleCenterY);
				float dist = sqrt(dot(uv,uv));
				float4 newColor = _MainColor;
				float border = 0.5-_CircleRadius;
				float t = 1.0 + smoothstep(_CircleRadius,_CircleRadius+border,dist)
							-smoothstep(_CircleRadius-border,_CircleRadius,dist);
				float percentdist =clamp(abs(_CircleRadius-dist)*9/_CircleRadius,0,1);
				//percentdist *= 0.8;
				t = 1-t;
				newColor.a = t;
				newColor.rgb = _MainColor.rgb * t;
				//newColor.rgb = _MainColor.rgb * (1-percentdist);
				float r = smoothstep(1.00,newColor.r,percentdist);
				float g = smoothstep(1.00,newColor.g,percentdist);
				float b = smoothstep(1.00,newColor.b,percentdist);
				newColor.rgb += float3(r,g,b);
				//newColor.a = 1;
				return newColor;
			}

			//void surf (Input IN, inout SurfaceOutput o) {
			//	float2 uv = float2(IN.uv.x , IN.uv.y );
			//	float dist = sqrt(dot(uv,uv));
				
				//if((dist > (_CircleRadius + _CircleBorder)) ||
				// (dist < (_CircleRadius + _CircleBorder)))
				//{
				//	o.Albedo = float3(0,0,0);
				//	o.Alpha = 0;
				//}
				//else
				//{
				//	o.Albedo = _MainColor.rgb;
				//	o.Alpha = 1;
				//}
				
			//	o.Alpha = 1;
			//	o.Albedo = _MainColor.rgb * IN.uv.y;
			//}
			ENDCG
		} 
	}
	FallBack "Diffuse"
}
