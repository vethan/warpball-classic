﻿Shader "Custom/Disk-Border" {
	Properties {
		[PerRendererData]_MainColor ("Color", Color) = (1,1,1,1)
		_CircleCenterX ("Circle Center X", Range(0,1)) = 0.5
		_CircleCenterY ("Circle Center Y", Range(0,1)) = 0.5
		_CircleRadius ("Radius Size",Range(0,1)) = 0.5
		_CircleBorder ("Border Size",Range(0,0.5)) = 0.0
	}
	SubShader 
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		Blend SrcAlpha One
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			
			sampler2D _MainTex;
			float _CircleCenterX;
			float _CircleCenterY;
			float _CircleRadius;
			float _CircleBorder;
			float4 _MainColor;
			
			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};

			fragmentInput vert(appdata_base i)
			{
				fragmentInput o;
				o.pos = mul(UNITY_MATRIX_MVP,i.vertex);
				o.uv = i.texcoord.xy;
				return o;
			}

			half4 frag(fragmentInput i) : COLOR
			{
				float2 uv = float2(i.uv.x -  _CircleCenterX, i.uv.y - _CircleCenterY);
				float dist = sqrt(dot(uv,uv));
				float4 newColor = _MainColor;
				float t = 1.0 + smoothstep(_CircleRadius,_CircleRadius+_CircleBorder,dist)
							-smoothstep(_CircleRadius,_CircleRadius-_CircleBorder,dist);
				
				t = 1-t;
				newColor.a = t;
				newColor.rgb = _MainColor.rgb * t;

				return newColor;
			}
			ENDCG
		} 
	}
	FallBack "Diffuse"
}
