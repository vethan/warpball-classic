﻿Shader "Unlit Bloom Only" {

 

	Properties {

	    _Color ("Color", Color) = (1,1,1,1)

	}

	 
	SubShader {
		Tags { "RenderType" = "Opaque"  }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		float4 _Color;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = _Color;
			o.Albedo = c.rgb*5;
			o.Alpha = c.a*5;
			o.Emission = c.rgb*5;
		}
		ENDCG
	}  

 }
