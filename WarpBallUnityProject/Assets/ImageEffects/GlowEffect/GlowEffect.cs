﻿using UnityEngine;
using System.Collections;

public class GlowEffect : MonoBehaviour {
	public Material imageGlowMaterial;
	RenderTexture lastFrame = null;
	void OnRenderImage(RenderTexture src, RenderTexture dest) 
	{
		if(lastFrame == null)
		{
			lastFrame = (RenderTexture)Instantiate(src);
		}

		imageGlowMaterial.SetTexture("_LastFrameTex",lastFrame);

		Graphics.Blit(src,dest,imageGlowMaterial);

		lastFrame = (RenderTexture)Instantiate(src);
	}
}
